﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Suppliers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Products
{
    [Table("MenuItemType", Schema = "dbo")]
    public class MenuItemTypes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int MenuItemTypeId { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string MenuItemTypeName { get; set; }
       
    }
}
