﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Inventory.Stocks;
using InventorySystemThesis.Models.Inventory.UoMs;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Products
{
    [Table("MenuItem", Schema = "dbo")]
    public class MenuItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int MenuItemId { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string MenuItemNameEN { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string MenuItemNameKH { get; set; }


        [Column(TypeName = "float")]
        public float Price { get; set; }


        [Column(TypeName = "float")]
        public float qtytoStock { get; set; }

        //public int UnitOfMeasureId { get; set; }
        //public virtual UnitOfMeasure UnitOfMeasure { get; set; }

        //public int MenuItemTypeId { get; set; }
        //public virtual MenuItemType MenuItemType { get; set; }

        //public int CategoryCateID { get; set; }
        //public virtual Category Category { get; set; }

        public int StockId { get; set; }
        public virtual Stock Stock { get; set; }

        public string updateBy { get; set; }
        public DateTime? updateAt { get; set; }


    }
}
