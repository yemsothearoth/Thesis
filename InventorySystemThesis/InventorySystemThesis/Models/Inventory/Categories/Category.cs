﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Categories
{
    [Table("Category", Schema = "dbo")]
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int CateID { get; set; }

        [Column(TypeName = "varchar(125)")]
        [Required]
        public string CateName { get; set; }
        
        [Column(TypeName = "nvarchar(125)")]
        public string CateNameKH { get; set; }

        [Column(TypeName = "varchar(5)")]
        public string Status { get; set; }


    }
}
