﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.UoMs
{
    [Table("UnitOfMeasure", Schema = "dbo")]
    public class UnitOfMeasure
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int UnitOfMeasureID { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string NameEN { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string NameKH { get; set; }
        
    }
}
