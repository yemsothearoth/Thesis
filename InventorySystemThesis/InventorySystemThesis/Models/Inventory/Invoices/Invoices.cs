﻿using InventorySystemThesis.Models.Inventory.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Invoices
{
    [Table("Invoice", Schema = "dbo")]
    public class Invoices
    {
        [Key]
        [Required]
        public string InvoicesId { get; set; }
        
       
        [Column(TypeName = "float")]
        public float Tax{ get; set; }

        [Column(TypeName = "float")]
        public float Discount { get; set; }

        [Column(TypeName = "float")]
        [Required]
        public float TotalPaidUSD{ get; set; }

        [Column(TypeName = "float")]
        public float TotalPaidKHR { get; set; }

        [Column(TypeName = "float")]
        [Required]
        public float RecievedAmountUSD{ get; set; }

        [Column(TypeName = "float")]
        public float RecievedAmountKHR { get; set; }

        [Column(TypeName = "float")]
        public float ChangedAmountUSD{ get; set; }

        [Column(TypeName = "float")]
        public float ChangedAmountKHR{ get; set; }

        [Column(TypeName = "datetime")]
        public DateTime createAt{ get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string createBy { get; set; }
        
        [Column(TypeName = "datetime")]
        public DateTime? updateAt { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string? updateBy { get; set; }
   

        [Column(TypeName = "varchar(5)")]
        public string Status { get; set; }

        //[Column(TypeName = "varchar(125)")]
        //public string By { get; set; }
        //public int TablesTableID { get; set; }

        //public virtual Tables Tables { get; set; }
        public int TableID { get; set; }

        public virtual Tables Table { get; set; }

        //public int InvoicesDetailId { get; set; }
        public List<InvoicesDetail> InvoicesDetail { get; set; }


    }
}
