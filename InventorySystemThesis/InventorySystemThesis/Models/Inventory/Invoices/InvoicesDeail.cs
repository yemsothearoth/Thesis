﻿using InventorySystemThesis.Models.Inventory.Products;
using InventorySystemThesis.Models.Inventory.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Invoices
{
    [Table("InvoiceDetail", Schema = "dbo")]
    public class InvoicesDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int InvoicesDetailId { get; set; }

        [Column(TypeName = "float")]
        public float Qty { get; set; }

        [Column(TypeName = "float")]
        public float Discount { get; set; }

        [Column(TypeName = "float")]
        public float Price { get; set; }
        //[Required]
        //public string InvoicesId { get; set; }
        //public virtual Invoices Invoices { get; set; }
        public int MenuItemId {get;set;}
        public virtual MenuItem MenuItem { get; set; }



    }
}
