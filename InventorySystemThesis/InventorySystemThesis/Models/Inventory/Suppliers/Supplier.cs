﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Suppliers
{
    [Table("Supplier", Schema = "dbo")]
    public class Supplier
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int SupplierID { get; set; }
        [Column(TypeName = "varchar(125)")]
        [Required]
        public string SupplierName { get; set; }
        [Column(TypeName = "varchar(125)")]
        public string Supplierphone { get; set; }
        [Column(TypeName = "varchar(125)")]
        public string SupplierAddress { get; set; }
        [Column(TypeName = "varchar(125)")]
        public string SupplierDescription { get; set; }

        [Column(TypeName = "varchar(5)")]
        public string Status { get; set; }
    }
}
