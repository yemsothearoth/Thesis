﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Inventory.UoMs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Stocks
{
    [Table("Stock", Schema = "dbo")]
    public class Stock
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int StockId { get; set; }


        [Column(TypeName = "nvarchar(125)")]
        public string StockNameEN { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string StockNameKH { get; set; }
        
        [Column(TypeName = "float")]
        public float StockQty { get; set; }

        public int CategoryCateID { get; set; }
        public virtual Category Category { get; set; }
        [Column(TypeName = "nvarchar(10)")]
        public string Status { get; set; }
        public string updateBy { get; set; }
        public DateTime? updateAt { get; set; }

        public int UnitOfMeasureId { get; set; }
        public virtual UnitOfMeasure UnitOfMeasure { get; set; }
    }
}
