﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Table
{
    [Table("Tables", Schema = "dbo")]
    public class Tables
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int? TableID { get; set; }
       
        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string TableNumber { get; set; }

        [Column(TypeName = "int")]
        [Required]
        public int FloorNumber { get; set; }

        [Column(TypeName = "nvarchar(5)")]
        [Required]
        public string Status { get; set; }




    }
}
