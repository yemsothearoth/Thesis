﻿using InventorySystemThesis.Models.Inventory.Table;
using InventorySystemThesis.Models.Inventory.TakeAways;
using InventorySystemThesis.Models.Inventory.TakeAwaysInvoices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.TakeAwaysInvoices
{
    [Table("TakeAwaysInvoice", Schema = "dbo")]
    public class TAInvoices
    {
        [Key]
        [Required]
        public string InvoicesId { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string? CustomerName { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string? PhoneNumber { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string? Address { get; set; }

        [Column(TypeName = "float")]
        public float Tax{ get; set; }

        [Column(TypeName = "float")]
        public float Discount { get; set; }

        [Column(TypeName = "float")]
        [Required]
        public float TotalPaidUSD{ get; set; }

        [Column(TypeName = "float")]
        public float TotalPaidKHR { get; set; }

        [Column(TypeName = "float")]
        [Required]
        public float RecievedAmountUSD{ get; set; }

        [Column(TypeName = "float")]
        public float RecievedAmountKHR { get; set; }

        [Column(TypeName = "float")]
        public float ChangedAmountUSD{ get; set; }

        [Column(TypeName = "float")]
        public float ChangedAmountKHR{ get; set; }

        [Column(TypeName = "datetime")]
        public DateTime createAt{ get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string createBy { get; set; }
        
        [Column(TypeName = "datetime")]
        public DateTime? updateAt { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        public string? updateBy { get; set; }
   

        [Column(TypeName = "varchar(5)")]
        public string Status { get; set; }

        public int TakeAwayId { get; set; }

        public virtual TakeAway TakeAway { get; set; }

        public List<TAInvoicesDetail> InvoicesDetail { get; set; }


    }
}
