﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.SystemConfigs
{
    [Table("SystemConfig", Schema = "dbo")]
    public class SystemConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int? Id { get; set; }
       
        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string Key { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string Value { get; set; }


    }
}
