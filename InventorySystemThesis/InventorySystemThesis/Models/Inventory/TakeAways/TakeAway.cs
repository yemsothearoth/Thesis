﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.TakeAways
{
    [Table("TakeAways", Schema = "dbo")]
    public class TakeAway
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        public int? Id { get; set; }
       
        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string Name { get; set; }

        [Column(TypeName = "nvarchar(5)")]
        [Required]
        public string Status { get; set; }

    }
}
