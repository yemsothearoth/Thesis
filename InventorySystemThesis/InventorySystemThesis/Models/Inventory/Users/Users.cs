﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Users
{
    [Table("Users", Schema = "dbo")]
    public class Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int UserID { get; set; }
       
        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string Username { get; set; }

        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string Password { get; set; }


        [Column(TypeName = "nvarchar(10)")]
        public string Gender { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime loginAt { get; set; }

        
        [Column(TypeName = "datetime")]
        public DateTime? logoutAt { get; set; }


        [Column(TypeName = "nvarchar(5)")]
        public string Status { get; set; }

        public int RolesID { get; set; }
        public virtual Roles Roles { get; set; }
    }
}
