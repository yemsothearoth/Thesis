﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Inventory.Users
{
    [Table("Roles", Schema = "dbo")]
    public class Roles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "int")]
        [Required]
        public int RolesID { get; set; }
       
        [Column(TypeName = "nvarchar(125)")]
        [Required]
        public string RolesName { get; set; }

    }
}
