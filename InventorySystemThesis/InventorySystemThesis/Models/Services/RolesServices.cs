﻿using InventorySystemThesis.Models.Inventory.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class RolesServices
    {
        private readonly AppContexts _contexts;
        public RolesServices(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<IEnumerable<Roles>> RolesList()
        {
            return await _contexts.Roles.ToListAsync();
        }
    }
}
