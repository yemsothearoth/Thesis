﻿using InventorySystemThesis.Models.Inventory.TakeAways;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class TakeAwaysServices
    {
        private readonly AppContexts _contexts;

        public TakeAwaysServices(AppContexts contexts)
        {
            _contexts = contexts;
        }
        public async Task<IEnumerable<TakeAway>> TakeAwaysList()
        {
            return await _contexts.TakeAways.ToListAsync();
        }
    }
}
