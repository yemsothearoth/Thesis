﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Inventory.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class CategoryServices
    {
        private readonly AppContexts _contexts;
        public CategoryServices(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<IEnumerable<Category>> CateList()
        {
            return await _contexts.Category.ToListAsync();
        }
    }
}
