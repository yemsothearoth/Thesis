﻿using InventorySystemThesis.Models.Inventory.Table;
using InventorySystemThesis.Models.Inventory.Users;
using InventorySystemThesis.Models.Inventory.Products;
using Microsoft.EntityFrameworkCore;
using InventorySystemThesis.Models.Inventory.Invoices;
using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Inventory.Stocks;
using InventorySystemThesis.Models.Inventory.UoMs;
using System.Threading.Tasks;
using System;
using InventorySystemThesis.Models.Inventory.SystemConfigs;
using InventorySystemThesis.Models.Inventory.TakeAways;
using InventorySystemThesis.Models.Inventory.TakeAwaysInvoices;

namespace InventorySystemThesis.Models.Services
{
    public class AppContexts : DbContext
    {
        public AppContexts(DbContextOptions<AppContexts> options) : base(options)
        {
        }
        public virtual DbSet<Tables> Tables { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Invoices> Invoices { get; set; }

        public virtual DbSet<InvoicesDetail> InvoicesDetails { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<MenuItemTypes> MenuItemTypes { get; set; }
        public virtual DbSet<Stock> Stocks { get;set;}
        public virtual DbSet<UnitOfMeasure> UnitOfMeasures { get; set; }
        public virtual DbSet<SystemConfig> SystemConfigs { get; set; }
        public virtual DbSet<TakeAway> TakeAways { get; set; }
        public virtual DbSet<TAInvoices> TAInvoices { get; set; }
        public virtual DbSet<TAInvoicesDetail> TAInvoicesDetails { get; set; }
        //maybe remove
        //public virtual DbSet<Supplier> Supplier { get; set; }
    }
}
