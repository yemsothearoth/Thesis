﻿using InventorySystemThesis.Models.Inventory.Products;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class MenuItemsServices
    {
        private readonly AppContexts _contexts;
        public MenuItemsServices(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<IEnumerable<MenuItem>> MenuItemList()
        {
            return await _contexts.MenuItems
                .Include(_=>_.Stock).ThenInclude(_=>_.Category)
                .Include(_=>_.Stock).ThenInclude(_=>_.UnitOfMeasure)
                .ToListAsync();
        }
        public async Task<MenuItem> FindMenuItemById(int id)
        {
            return await _contexts.MenuItems
                .Include(_ => _.Stock).ThenInclude(_ => _.Category)
                .Include(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                .Where(_=>_.MenuItemId==id)
                .FirstOrDefaultAsync();
        }
    }
}
