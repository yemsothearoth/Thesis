﻿using InventorySystemThesis.Models.Inventory.Stocks;
using InventorySystemThesis.Models.Inventory.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class StocksServices
    {
        private readonly AppContexts _contexts;
        public StocksServices(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<IEnumerable<Stock>> StockList()
        {
            return await _contexts.Stocks.ToListAsync();
        }
    }
}
