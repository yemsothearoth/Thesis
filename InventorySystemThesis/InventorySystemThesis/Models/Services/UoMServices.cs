﻿using InventorySystemThesis.Models.Inventory.UoMs;
using InventorySystemThesis.Models.Inventory.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class UoMServices
    {
        private readonly AppContexts _contexts;
        public UoMServices(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<IEnumerable<UnitOfMeasure>> UoMList()
        {
            return await _contexts.UnitOfMeasures.ToListAsync();
        }
    }
}
