﻿using InventorySystemThesis.Models.Inventory.Table;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class TableServices
    {
        private readonly AppContexts _contexts;

        public TableServices(AppContexts contexts)
        {
            _contexts = contexts;
        }
        public async Task<IEnumerable<Tables>> TableList()
        {
            return await _contexts.Tables.ToListAsync();
        }
    }
}
