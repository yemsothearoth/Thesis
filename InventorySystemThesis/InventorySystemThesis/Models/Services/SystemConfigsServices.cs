﻿using InventorySystemThesis.Models.Inventory.SystemConfigs;
using InventorySystemThesis.Models.Inventory.Table;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Models.Services
{
    public class SystemConfigsServices
    {
        private readonly AppContexts _contexts;

        public SystemConfigsServices(AppContexts contexts)
        {
            _contexts = contexts;
        }
        public async Task<IEnumerable<SystemConfig>> SystemConfigList()
        {
            return await _contexts.SystemConfigs.ToListAsync();
        }
    }
}
