﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Category",
                schema: "dbo",
                columns: table => new
                {
                    CateID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CateName = table.Column<string>(type: "varchar(125)", nullable: false),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.CateID);
                });

            migrationBuilder.CreateTable(
                name: "Invoice",
                schema: "dbo",
                columns: table => new
                {
                    InvID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableNo = table.Column<int>(type: "int", nullable: false),
                    FloorNumber = table.Column<int>(type: "int", nullable: false),
                    InvCode = table.Column<string>(type: "varchar(125)", nullable: false),
                    Tax = table.Column<double>(type: "float", nullable: false),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    TotalPaid = table.Column<double>(type: "float", nullable: false),
                    RecievedAmount = table.Column<double>(type: "float", nullable: false),
                    ChangedAmount = table.Column<double>(type: "float", nullable: false),
                    InvDate = table.Column<DateTime>(type: "date", nullable: false),
                    InvTime = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    By = table.Column<string>(type: "varchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoice", x => x.InvID);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                schema: "dbo",
                columns: table => new
                {
                    ProductID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductName = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    ProductQty = table.Column<double>(type: "float", nullable: false),
                    ProductUoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    PriceIn = table.Column<double>(type: "float", nullable: false),
                    UnitPrice = table.Column<double>(type: "float", nullable: false),
                    CategoryName = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    SupplierName = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    lastUpdateBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    lastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(type: "varchar(125)", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductID);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                schema: "dbo",
                columns: table => new
                {
                    SaleID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableID = table.Column<int>(type: "int", nullable: false),
                    FloorNumber = table.Column<int>(type: "int", nullable: false),
                    InvCode = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    ProductName = table.Column<string>(type: "varchar(125)", nullable: false),
                    Qty = table.Column<double>(type: "float", nullable: false),
                    UoM = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    UnitPrice = table.Column<double>(type: "float", nullable: false),
                    TotalPriceUSD = table.Column<double>(type: "float", nullable: false),
                    TotalPriceKHR = table.Column<double>(type: "float", nullable: false),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.SaleID);
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                schema: "dbo",
                columns: table => new
                {
                    SupplierID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SupplierName = table.Column<string>(type: "varchar(125)", nullable: false),
                    Supplierphone = table.Column<string>(type: "varchar(125)", nullable: true),
                    SupplierAddress = table.Column<string>(type: "varchar(125)", nullable: true),
                    SupplierDescription = table.Column<string>(type: "varchar(125)", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.SupplierID);
                });

            migrationBuilder.CreateTable(
                name: "Tables",
                schema: "dbo",
                columns: table => new
                {
                    TableID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableNumber = table.Column<int>(type: "int", nullable: false),
                    FloorNumber = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: false),
                    Description = table.Column<string>(type: "varchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tables", x => x.TableID);
                });

            migrationBuilder.CreateTable(
                name: "UoM",
                schema: "dbo",
                columns: table => new
                {
                    UoMId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UoMNameEn = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    UoMNameKH = table.Column<string>(type: "nvarchar(255)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UoM", x => x.UoMId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "dbo",
                columns: table => new
                {
                    UserID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    Role = table.Column<string>(type: "nvarchar(10)", nullable: true),
                    loginAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    logoutAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    Description = table.Column<string>(type: "varchar(125)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Category",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Invoice",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Product",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Sales",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Supplier",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Tables",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "UoM",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "dbo");
        }
    }
}
