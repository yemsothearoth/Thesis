﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0027 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropTable(
                name: "Stock",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<int>(
                name: "StockCId",
                schema: "dbo",
                table: "MenuItem",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StockC",
                schema: "dbo",
                columns: table => new
                {
                    StockCId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    stockNameEN = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockNameKH = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockQty = table.Column<double>(type: "float", nullable: false),
                    createBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    createAt = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockUoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    CategoryCateID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockC", x => x.StockCId);
                    table.ForeignKey(
                        name: "FK_StockC_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StockCId",
                schema: "dbo",
                table: "MenuItem",
                column: "StockCId");

            migrationBuilder.CreateIndex(
                name: "IX_StockC_CategoryCateID",
                schema: "dbo",
                table: "StockC",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_StockC_StockCId",
                schema: "dbo",
                table: "MenuItem",
                column: "StockCId",
                principalSchema: "dbo",
                principalTable: "StockC",
                principalColumn: "StockCId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_StockC_StockCId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropTable(
                name: "StockC",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StockCId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StockCId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<int>(
                name: "StockID",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Stock",
                schema: "dbo",
                columns: table => new
                {
                    StockID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryCateID = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    createAt = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    createBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockNameEN = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockNameKH = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockQty = table.Column<double>(type: "float", nullable: false),
                    stockUoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    updateBy = table.Column<string>(type: "nvarchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.StockID);
                    table.ForeignKey(
                        name: "FK_Stock_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StockID",
                schema: "dbo",
                table: "MenuItem",
                column: "StockID");

            migrationBuilder.CreateIndex(
                name: "IX_Stock_CategoryCateID",
                schema: "dbo",
                table: "Stock",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StockID",
                schema: "dbo",
                table: "MenuItem",
                column: "StockID",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StockID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
