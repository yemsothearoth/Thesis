﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0041 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvDate",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "InvTime",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.RenameColumn(
                name: "updateTime",
                schema: "dbo",
                table: "Invoice",
                newName: "createBy");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updateAt",
                schema: "dbo",
                table: "Invoice",
                type: "datetime",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "date",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "createAt",
                schema: "dbo",
                table: "Invoice",
                type: "datetime",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "createAt",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.RenameColumn(
                name: "createBy",
                schema: "dbo",
                table: "Invoice",
                newName: "updateTime");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updateAt",
                schema: "dbo",
                table: "Invoice",
                type: "date",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InvDate",
                schema: "dbo",
                table: "Invoice",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "InvTime",
                schema: "dbo",
                table: "Invoice",
                type: "nvarchar(125)",
                nullable: false,
                defaultValue: "");
        }
    }
}
