﻿// <auto-generated />
using System;
using InventorySystemThesis.Models.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace WebApplication2.Migrations
{
    [DbContext(typeof(AppContexts))]
    [Migration("20210202115229_dbv0.0.43")]
    partial class dbv0043
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Categories.Category", b =>
                {
                    b.Property<int>("CateID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("CateName")
                        .IsRequired()
                        .HasColumnType("varchar(125)");

                    b.Property<string>("CateNameKH")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(5)");

                    b.HasKey("CateID");

                    b.ToTable("Category", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Invoices.Invoices", b =>
                {
                    b.Property<string>("InvoicesId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<double>("ChangedAmountKHR")
                        .HasColumnType("float");

                    b.Property<double>("ChangedAmountUSD")
                        .HasColumnType("float");

                    b.Property<double>("Discount")
                        .HasColumnType("float");

                    b.Property<double>("RecievedAmountKHR")
                        .HasColumnType("float");

                    b.Property<double>("RecievedAmountUSD")
                        .HasColumnType("float");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(5)");

                    b.Property<int>("TableID")
                        .HasColumnType("int");

                    b.Property<double>("Tax")
                        .HasColumnType("float");

                    b.Property<double>("TotalPaidKHR")
                        .HasColumnType("float");

                    b.Property<double>("TotalPaidUSD")
                        .HasColumnType("float");

                    b.Property<DateTime>("createAt")
                        .HasColumnType("datetime");

                    b.Property<string>("createBy")
                        .HasColumnType("nvarchar(125)");

                    b.Property<DateTime?>("updateAt")
                        .HasColumnType("datetime");

                    b.Property<string>("updateBy")
                        .HasColumnType("nvarchar(125)");

                    b.HasKey("InvoicesId");

                    b.HasIndex("TableID");

                    b.ToTable("Invoice", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Invoices.InvoicesDetail", b =>
                {
                    b.Property<int>("InvoicesDetailId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<double>("Discount")
                        .HasColumnType("float");

                    b.Property<string>("InvoicesId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("MenuItemId")
                        .HasColumnType("int");

                    b.Property<double>("Price")
                        .HasColumnType("float");

                    b.Property<double>("Qty")
                        .HasColumnType("float");

                    b.HasKey("InvoicesDetailId");

                    b.HasIndex("InvoicesId");

                    b.HasIndex("MenuItemId");

                    b.ToTable("InvoiceDetail", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Products.MenuItem", b =>
                {
                    b.Property<int>("MenuItemId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("MenuItemNameEN")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("MenuItemNameKH")
                        .HasColumnType("nvarchar(125)");

                    b.Property<double>("Price")
                        .HasColumnType("float");

                    b.Property<int>("StockId")
                        .HasColumnType("int");

                    b.Property<double>("qtytoStock")
                        .HasColumnType("float");

                    b.Property<DateTime?>("updateAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("updateBy")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("MenuItemId");

                    b.HasIndex("StockId");

                    b.ToTable("MenuItem", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Products.MenuItemTypes", b =>
                {
                    b.Property<int>("MenuItemTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("MenuItemTypeName")
                        .HasColumnType("nvarchar(125)");

                    b.HasKey("MenuItemTypeId");

                    b.ToTable("MenuItemType", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Products.Product", b =>
                {
                    b.Property<int>("ProductID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int?>("CategoryCateID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(125)");

                    b.Property<string>("ProductName")
                        .IsRequired()
                        .HasColumnType("nvarchar(125)");

                    b.Property<double>("ProductQty")
                        .HasColumnType("float");

                    b.Property<string>("ProductUoM")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(5)");

                    b.Property<int?>("SupplierID")
                        .HasColumnType("int");

                    b.Property<double>("UnitPrice")
                        .HasColumnType("float");

                    b.Property<string>("lastUpdateBy")
                        .HasColumnType("nvarchar(125)");

                    b.Property<DateTime>("lastUpdateDateTime")
                        .HasColumnType("datetime");

                    b.HasKey("ProductID");

                    b.HasIndex("CategoryCateID");

                    b.HasIndex("SupplierID");

                    b.ToTable("Product", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Sales.Sales", b =>
                {
                    b.Property<int>("SaleID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<double>("Discount")
                        .HasColumnType("float");

                    b.Property<string>("InvCode")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("ProductName")
                        .HasColumnType("varchar(125)");

                    b.Property<double>("Qty")
                        .HasColumnType("float");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(5)");

                    b.Property<int?>("TablesTableID")
                        .HasColumnType("int");

                    b.Property<double>("TotalPriceKHR")
                        .HasColumnType("float");

                    b.Property<double>("TotalPriceUSD")
                        .HasColumnType("float");

                    b.Property<double>("UnitPrice")
                        .HasColumnType("float");

                    b.Property<string>("UoM")
                        .HasColumnType("nvarchar(125)");

                    b.HasKey("SaleID");

                    b.HasIndex("TablesTableID");

                    b.ToTable("Sales", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Sales.SalesMenu", b =>
                {
                    b.Property<int>("SalesMenuID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("SalesMenuName")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(5)");

                    b.Property<double>("UnitPrice")
                        .HasColumnType("float");

                    b.HasKey("SalesMenuID");

                    b.ToTable("SalesMenu", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Stocks.Stock", b =>
                {
                    b.Property<int>("StockId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("CategoryCateID")
                        .HasColumnType("int");

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(10)");

                    b.Property<string>("StockNameEN")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("StockNameKH")
                        .HasColumnType("nvarchar(125)");

                    b.Property<double>("StockQty")
                        .HasColumnType("float");

                    b.Property<int>("UnitOfMeasureId")
                        .HasColumnType("int");

                    b.Property<DateTime?>("updateAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("updateBy")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("StockId");

                    b.HasIndex("CategoryCateID");

                    b.HasIndex("UnitOfMeasureId");

                    b.ToTable("Stock", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Table.Tables", b =>
                {
                    b.Property<int?>("TableID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("FloorNumber")
                        .HasColumnType("int");

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasColumnType("nvarchar(5)");

                    b.Property<int>("TableNumber")
                        .HasColumnType("int");

                    b.HasKey("TableID");

                    b.ToTable("Tables", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.UoMs.UnitOfMeasure", b =>
                {
                    b.Property<int>("UnitOfMeasureID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("NameEN")
                        .HasColumnType("nvarchar(125)");

                    b.Property<string>("NameKH")
                        .HasColumnType("nvarchar(125)");

                    b.HasKey("UnitOfMeasureID");

                    b.ToTable("UnitOfMeasure", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Users.Roles", b =>
                {
                    b.Property<int>("RolesID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("RolesName")
                        .IsRequired()
                        .HasColumnType("nvarchar(125)");

                    b.HasKey("RolesID");

                    b.ToTable("Roles", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Users.Users", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Gender")
                        .HasColumnType("nvarchar(10)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(125)");

                    b.Property<int>("RolesID")
                        .HasColumnType("int");

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(5)");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("nvarchar(125)");

                    b.Property<DateTime>("loginAt")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("logoutAt")
                        .HasColumnType("datetime");

                    b.HasKey("UserID");

                    b.HasIndex("RolesID");

                    b.ToTable("Users", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Suppliers.Supplier", b =>
                {
                    b.Property<int>("SupplierID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Status")
                        .HasColumnType("varchar(5)");

                    b.Property<string>("SupplierAddress")
                        .HasColumnType("varchar(125)");

                    b.Property<string>("SupplierDescription")
                        .HasColumnType("varchar(125)");

                    b.Property<string>("SupplierName")
                        .IsRequired()
                        .HasColumnType("varchar(125)");

                    b.Property<string>("Supplierphone")
                        .HasColumnType("varchar(125)");

                    b.HasKey("SupplierID");

                    b.ToTable("Supplier", "dbo");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Invoices.Invoices", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Table.Tables", "Table")
                        .WithMany()
                        .HasForeignKey("TableID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Table");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Invoices.InvoicesDetail", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Invoices.Invoices", null)
                        .WithMany("InvoicesDetail")
                        .HasForeignKey("InvoicesId");

                    b.HasOne("InventorySystemThesis.Models.Inventory.Products.MenuItem", "MenuItem")
                        .WithMany()
                        .HasForeignKey("MenuItemId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("MenuItem");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Products.MenuItem", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Stocks.Stock", "Stock")
                        .WithMany()
                        .HasForeignKey("StockId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Stock");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Products.Product", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Categories.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryCateID");

                    b.HasOne("InventorySystemThesis.Models.Suppliers.Supplier", "Supplier")
                        .WithMany()
                        .HasForeignKey("SupplierID");

                    b.Navigation("Category");

                    b.Navigation("Supplier");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Sales.Sales", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Table.Tables", "Tables")
                        .WithMany()
                        .HasForeignKey("TablesTableID");

                    b.Navigation("Tables");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Stocks.Stock", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Categories.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryCateID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventorySystemThesis.Models.Inventory.UoMs.UnitOfMeasure", "UnitOfMeasure")
                        .WithMany()
                        .HasForeignKey("UnitOfMeasureId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Category");

                    b.Navigation("UnitOfMeasure");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Users.Users", b =>
                {
                    b.HasOne("InventorySystemThesis.Models.Inventory.Users.Roles", "Roles")
                        .WithMany()
                        .HasForeignKey("RolesID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Roles");
                });

            modelBuilder.Entity("InventorySystemThesis.Models.Inventory.Invoices.Invoices", b =>
                {
                    b.Navigation("InvoicesDetail");
                });
#pragma warning restore 612, 618
        }
    }
}
