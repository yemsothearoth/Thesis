﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0030 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID"
                );
              //  onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem");
        }
    }
}
