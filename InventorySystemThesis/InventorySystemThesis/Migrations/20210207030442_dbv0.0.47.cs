﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0047 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TakeAways",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TakeAways", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TakeAwaysInvoice",
                schema: "dbo",
                columns: table => new
                {
                    InvoicesId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CustomerName = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Tax = table.Column<double>(type: "float", nullable: false),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    TotalPaidUSD = table.Column<double>(type: "float", nullable: false),
                    TotalPaidKHR = table.Column<double>(type: "float", nullable: false),
                    RecievedAmountUSD = table.Column<double>(type: "float", nullable: false),
                    RecievedAmountKHR = table.Column<double>(type: "float", nullable: false),
                    ChangedAmountUSD = table.Column<double>(type: "float", nullable: false),
                    ChangedAmountKHR = table.Column<double>(type: "float", nullable: false),
                    createAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    createBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    updateBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    TableID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TakeAwaysInvoice", x => x.InvoicesId);
                    table.ForeignKey(
                        name: "FK_TakeAwaysInvoice_Tables_TableID",
                        column: x => x.TableID,
                        principalSchema: "dbo",
                        principalTable: "Tables",
                        principalColumn: "TableID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TakeAwaysInvoiceDetail",
                schema: "dbo",
                columns: table => new
                {
                    InvoicesDetailId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Qty = table.Column<double>(type: "float", nullable: false),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    MenuItemId = table.Column<int>(type: "int", nullable: false),
                    TAInvoicesInvoicesId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TakeAwaysInvoiceDetail", x => x.InvoicesDetailId);
                    table.ForeignKey(
                        name: "FK_TakeAwaysInvoiceDetail_MenuItem_MenuItemId",
                        column: x => x.MenuItemId,
                        principalSchema: "dbo",
                        principalTable: "MenuItem",
                        principalColumn: "MenuItemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TakeAwaysInvoiceDetail_TakeAwaysInvoice_TAInvoicesInvoicesId",
                        column: x => x.TAInvoicesInvoicesId,
                        principalSchema: "dbo",
                        principalTable: "TakeAwaysInvoice",
                        principalColumn: "InvoicesId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TakeAwaysInvoice_TableID",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                column: "TableID");

            migrationBuilder.CreateIndex(
                name: "IX_TakeAwaysInvoiceDetail_MenuItemId",
                schema: "dbo",
                table: "TakeAwaysInvoiceDetail",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_TakeAwaysInvoiceDetail_TAInvoicesInvoicesId",
                schema: "dbo",
                table: "TakeAwaysInvoiceDetail",
                column: "TAInvoicesInvoicesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TakeAways",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TakeAwaysInvoiceDetail",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TakeAwaysInvoice",
                schema: "dbo");
        }
    }
}
