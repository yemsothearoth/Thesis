﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0039 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                column: "MenuItemTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                column: "MenuItemTypeId",
                principalSchema: "dbo",
                principalTable: "MenuItemType",
                principalColumn: "MenuItemTypeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
