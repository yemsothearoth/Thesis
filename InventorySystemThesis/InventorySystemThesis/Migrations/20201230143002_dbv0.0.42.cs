﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0042 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID");

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                column: "UnitOfMeasureId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                column: "UnitOfMeasureId",
                principalSchema: "dbo",
                principalTable: "UnitOfMeasure",
                principalColumn: "UnitOfMeasureID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
