﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbc005 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalPaid",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<double>(
                name: "TotalPaidKHR",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalPaidUSD",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalPaidKHR",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "TotalPaidUSD",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<double>(
                name: "TotalPaid",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
