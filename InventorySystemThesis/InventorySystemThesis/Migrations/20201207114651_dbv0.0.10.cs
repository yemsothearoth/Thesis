﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0010 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "RolesID",
                schema: "dbo",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users",
                column: "RolesID",
                principalSchema: "dbo",
                principalTable: "Roles",
                principalColumn: "RolesID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "RolesID",
                schema: "dbo",
                table: "Users",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users",
                column: "RolesID",
                principalSchema: "dbo",
                principalTable: "Roles",
                principalColumn: "RolesID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
