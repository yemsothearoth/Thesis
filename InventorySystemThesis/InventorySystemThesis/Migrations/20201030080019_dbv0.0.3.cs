﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UoM",
                schema: "dbo",
                table: "Sales",
                type: "nvarchar(125)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(125)");

            migrationBuilder.AlterColumn<string>(
                name: "ProductName",
                schema: "dbo",
                table: "Sales",
                type: "varchar(125)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(125)");

            migrationBuilder.AlterColumn<string>(
                name: "InvCode",
                schema: "dbo",
                table: "Sales",
                type: "nvarchar(125)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(125)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UoM",
                schema: "dbo",
                table: "Sales",
                type: "nvarchar(125)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(125)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductName",
                schema: "dbo",
                table: "Sales",
                type: "varchar(125)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(125)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InvCode",
                schema: "dbo",
                table: "Sales",
                type: "nvarchar(125)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(125)",
                oldNullable: true);
        }
    }
}
