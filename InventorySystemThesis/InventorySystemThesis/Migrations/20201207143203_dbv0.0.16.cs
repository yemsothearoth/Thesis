﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0016 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AlterColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AlterColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
