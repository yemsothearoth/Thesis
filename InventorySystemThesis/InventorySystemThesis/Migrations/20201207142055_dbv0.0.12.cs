﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_MenuItem_MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_MenuItem_MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "MenuItemId",
                principalSchema: "dbo",
                principalTable: "MenuItem",
                principalColumn: "MenuItemId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_MenuItem_MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_MenuItem_MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "MenuItemId",
                principalSchema: "dbo",
                principalTable: "MenuItem",
                principalColumn: "MenuItemId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
