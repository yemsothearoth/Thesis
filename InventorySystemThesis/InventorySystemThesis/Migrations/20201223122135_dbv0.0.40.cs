﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0040 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.AlterColumn<string>(
                name: "InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<DateTime>(
                name: "updateAt",
                schema: "dbo",
                table: "Invoice",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "updateBy",
                schema: "dbo",
                table: "Invoice",
                type: "nvarchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "updateTime",
                schema: "dbo",
                table: "Invoice",
                type: "nvarchar(125)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "InvoicesId",
                principalSchema: "dbo",
                principalTable: "Invoice",
                principalColumn: "InvoicesId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.DropColumn(
                name: "updateAt",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "updateBy",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "updateTime",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AlterColumn<string>(
                name: "InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "InvoicesId",
                principalSchema: "dbo",
                principalTable: "Invoice",
                principalColumn: "InvoicesId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
