﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0024 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                schema: "dbo",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "stockweight",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "MenuItemName",
                schema: "dbo",
                table: "MenuItem",
                newName: "MenuItemNameKH");

            migrationBuilder.AlterColumn<DateTime>(
                name: "logoutAt",
                schema: "dbo",
                table: "Users",
                type: "datetime",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime");

            migrationBuilder.AddColumn<string>(
                name: "MenuItemNameEN",
                schema: "dbo",
                table: "MenuItem",
                type: "nvarchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StocksId",
                schema: "dbo",
                table: "MenuItem",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "qtytoStock",
                schema: "dbo",
                table: "MenuItem",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "Stock",
                schema: "dbo",
                columns: table => new
                {
                    StocksId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    stockNameEN = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockNameKH = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockQty = table.Column<double>(type: "float", nullable: false),
                    createBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    createAt = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    stockUoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    updateAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    CategoryCateID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.StocksId);
                    table.ForeignKey(
                        name: "FK_Stock_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StocksId",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksId");

            migrationBuilder.CreateIndex(
                name: "IX_Stock_CategoryCateID",
                schema: "dbo",
                table: "Stock",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StocksId",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksId",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StocksId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StocksId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropTable(
                name: "Stock",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StocksId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "MenuItemNameEN",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StocksId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "qtytoStock",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "MenuItemNameKH",
                schema: "dbo",
                table: "MenuItem",
                newName: "MenuItemName");

            migrationBuilder.AlterColumn<DateTime>(
                name: "logoutAt",
                schema: "dbo",
                table: "Users",
                type: "datetime",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Users",
                type: "varchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "stockweight",
                schema: "dbo",
                table: "MenuItem",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
