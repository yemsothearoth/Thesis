﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv006 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                schema: "dbo",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "FloorNumber",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "TableID",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "CategoryName",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "PriceIn",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "SupplierName",
                schema: "dbo",
                table: "Product");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Users",
                type: "varchar(125)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(125)");

            migrationBuilder.AddColumn<int>(
                name: "RolesID",
                schema: "dbo",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Discount",
                schema: "dbo",
                table: "Sales",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupplierID",
                schema: "dbo",
                table: "Product",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "dbo",
                columns: table => new
                {
                    RolesID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RolesName = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    Description = table.Column<string>(type: "varchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RolesID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_RolesID",
                schema: "dbo",
                table: "Users",
                column: "RolesID");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_TablesTableID",
                schema: "dbo",
                table: "Sales",
                column: "TablesTableID");

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryCateID",
                schema: "dbo",
                table: "Product",
                column: "CategoryCateID");

            migrationBuilder.CreateIndex(
                name: "IX_Product_SupplierID",
                schema: "dbo",
                table: "Product",
                column: "SupplierID");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Category_CategoryCateID",
                schema: "dbo",
                table: "Product",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Supplier_SupplierID",
                schema: "dbo",
                table: "Product",
                column: "SupplierID",
                principalSchema: "dbo",
                principalTable: "Supplier",
                principalColumn: "SupplierID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Tables_TablesTableID",
                schema: "dbo",
                table: "Sales",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users",
                column: "RolesID",
                principalSchema: "dbo",
                principalTable: "Roles",
                principalColumn: "RolesID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Category_CategoryCateID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Product_Supplier_SupplierID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Tables_TablesTableID",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Roles_RolesID",
                schema: "dbo",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_Users_RolesID",
                schema: "dbo",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Sales_TablesTableID",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Product_CategoryCateID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_SupplierID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "RolesID",
                schema: "dbo",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Discount",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "TablesTableID",
                schema: "dbo",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "CategoryCateID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "SupplierID",
                schema: "dbo",
                table: "Product");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Users",
                type: "varchar(125)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(125)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Role",
                schema: "dbo",
                table: "Users",
                type: "nvarchar(10)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FloorNumber",
                schema: "dbo",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TableID",
                schema: "dbo",
                table: "Sales",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CategoryName",
                schema: "dbo",
                table: "Product",
                type: "nvarchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PriceIn",
                schema: "dbo",
                table: "Product",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "SupplierName",
                schema: "dbo",
                table: "Product",
                type: "nvarchar(125)",
                nullable: true);
        }
    }
}
