﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0038 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UoM",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<int>(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                column: "UnitOfMeasureId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem",
                column: "UnitOfMeasureId",
                principalSchema: "dbo",
                principalTable: "UnitOfMeasure",
                principalColumn: "UnitOfMeasureID");
              //  ,onDelete: ReferentialAction.Cascade);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<string>(
                name: "UoM",
                schema: "dbo",
                table: "MenuItem",
                type: "nvarchar(125)",
                nullable: true);
        }
    }
}
