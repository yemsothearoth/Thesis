﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0046 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "SalesMenu",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Supplier",
                schema: "dbo");

            migrationBuilder.AlterColumn<string>(
                name: "TableNumber",
                schema: "dbo",
                table: "Tables",
                type: "nvarchar(125)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TableNumber",
                schema: "dbo",
                table: "Tables",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(125)");

            migrationBuilder.CreateTable(
                name: "SalesMenu",
                schema: "dbo",
                columns: table => new
                {
                    SalesMenuID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SalesMenuName = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    UnitPrice = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalesMenu", x => x.SalesMenuID);
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                schema: "dbo",
                columns: table => new
                {
                    SupplierID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    SupplierAddress = table.Column<string>(type: "varchar(125)", nullable: true),
                    SupplierDescription = table.Column<string>(type: "varchar(125)", nullable: true),
                    SupplierName = table.Column<string>(type: "varchar(125)", nullable: false),
                    Supplierphone = table.Column<string>(type: "varchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.SupplierID);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                schema: "dbo",
                columns: table => new
                {
                    ProductID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryCateID = table.Column<int>(type: "int", nullable: true),
                    Description = table.Column<string>(type: "varchar(125)", nullable: true),
                    ProductName = table.Column<string>(type: "nvarchar(125)", nullable: false),
                    ProductQty = table.Column<double>(type: "float", nullable: false),
                    ProductUoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    SupplierID = table.Column<int>(type: "int", nullable: true),
                    UnitPrice = table.Column<double>(type: "float", nullable: false),
                    lastUpdateBy = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    lastUpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductID);
                    table.ForeignKey(
                        name: "FK_Product_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_Supplier_SupplierID",
                        column: x => x.SupplierID,
                        principalSchema: "dbo",
                        principalTable: "Supplier",
                        principalColumn: "SupplierID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryCateID",
                schema: "dbo",
                table: "Product",
                column: "CategoryCateID");

            migrationBuilder.CreateIndex(
                name: "IX_Product_SupplierID",
                schema: "dbo",
                table: "Product",
                column: "SupplierID");
        }
    }
}
