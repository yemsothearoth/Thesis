﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0044 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sales",
                schema: "dbo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sales",
                schema: "dbo",
                columns: table => new
                {
                    SaleID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    InvCode = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    ProductName = table.Column<string>(type: "varchar(125)", nullable: true),
                    Qty = table.Column<double>(type: "float", nullable: false),
                    Status = table.Column<string>(type: "varchar(5)", nullable: true),
                    TablesTableID = table.Column<int>(type: "int", nullable: true),
                    TotalPriceKHR = table.Column<double>(type: "float", nullable: false),
                    TotalPriceUSD = table.Column<double>(type: "float", nullable: false),
                    UnitPrice = table.Column<double>(type: "float", nullable: false),
                    UoM = table.Column<string>(type: "nvarchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.SaleID);
                    table.ForeignKey(
                        name: "FK_Sales_Tables_TablesTableID",
                        column: x => x.TablesTableID,
                        principalSchema: "dbo",
                        principalTable: "Tables",
                        principalColumn: "TableID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_TablesTableID",
                schema: "dbo",
                table: "Sales",
                column: "TablesTableID");
        }
    }
}
