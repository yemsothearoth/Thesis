﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv00356 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "Stock",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Stock_UnitOfMeasureId",
                schema: "dbo",
                table: "Stock",
                column: "UnitOfMeasureId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stock_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "Stock",
                column: "UnitOfMeasureId",
                principalSchema: "dbo",
                principalTable: "UnitOfMeasure",
                principalColumn: "UnitOfMeasureID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stock_UnitOfMeasure_UnitOfMeasureId",
                schema: "dbo",
                table: "Stock");

            migrationBuilder.DropIndex(
                name: "IX_Stock_UnitOfMeasureId",
                schema: "dbo",
                table: "Stock");

            migrationBuilder.DropColumn(
                name: "UnitOfMeasureId",
                schema: "dbo",
                table: "Stock");
        }
    }
}
