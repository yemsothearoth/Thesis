﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv009 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Invoice",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "Description",
                schema: "dbo",
                table: "Tables");

            migrationBuilder.DropColumn(
                name: "Description",
                schema: "dbo",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "dbo",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "InvID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "FloorNumber",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "InvCode",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "TableNo",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<string>(
                name: "InvoicesId",
                schema: "dbo",
                table: "Invoice",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Invoice",
                schema: "dbo",
                table: "Invoice",
                column: "InvoicesId");

            migrationBuilder.CreateTable(
                name: "MenuItemType",
                schema: "dbo",
                columns: table => new
                {
                    MenuItemTypeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MenuItemTypeName = table.Column<string>(type: "nvarchar(125)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuItemType", x => x.MenuItemTypeId);
                });

            migrationBuilder.CreateTable(
                name: "MenuItem",
                schema: "dbo",
                columns: table => new
                {
                    MenuItemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MenuItemName = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    Price = table.Column<double>(type: "float", nullable: false),
                    stockweight = table.Column<double>(type: "float", nullable: false),
                    UoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    MenuItemTypeId = table.Column<int>(nullable: true),
                    CategoryCateID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuItem", x => x.MenuItemId);
                    table.ForeignKey(
                        name: "FK_MenuItem_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                        column: x => x.MenuItemTypeId,
                        principalSchema: "dbo",
                        principalTable: "MenuItemType",
                        principalColumn: "MenuItemTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceDetail",
                schema: "dbo",
                columns: table => new
                {
                    InvoicesDetailId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Qty = table.Column<double>(type: "float", nullable: false),
                    Discount = table.Column<double>(type: "float", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    InvoicesId = table.Column<string>(nullable: true),
                    MenuItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceDetail", x => x.InvoicesDetailId);
                    table.ForeignKey(
                        name: "FK_InvoiceDetail_Invoice_InvoicesId",
                        column: x => x.InvoicesId,
                        principalSchema: "dbo",
                        principalTable: "Invoice",
                        principalColumn: "InvoicesId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoiceDetail_MenuItem_MenuItemId",
                        column: x => x.MenuItemId,
                        principalSchema: "dbo",
                        principalTable: "MenuItem",
                        principalColumn: "MenuItemId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Invoice_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetail_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "InvoicesId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetail_MenuItemId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "MenuItemId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID");

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                column: "MenuItemTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropTable(
                name: "InvoiceDetail",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "MenuItem",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "MenuItemType",
                schema: "dbo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Invoice",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropIndex(
                name: "IX_Invoice_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "InvoicesId",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Tables",
                type: "varchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "dbo",
                table: "Roles",
                type: "varchar(125)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                schema: "dbo",
                table: "Roles",
                type: "nvarchar(5)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvID",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "FloorNumber",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "InvCode",
                schema: "dbo",
                table: "Invoice",
                type: "varchar(125)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "TableNo",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Invoice",
                schema: "dbo",
                table: "Invoice",
                column: "InvID");
        }
    }
}
