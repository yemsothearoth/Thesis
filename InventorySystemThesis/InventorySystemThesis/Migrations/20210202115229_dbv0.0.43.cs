﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0043 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "By",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.RenameColumn(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                newName: "TableID");

            migrationBuilder.RenameIndex(
                name: "IX_Invoice_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                newName: "IX_Invoice_TableID");

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TableID",
                schema: "dbo",
                table: "Invoice",
                column: "TableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.RenameColumn(
                name: "TableID",
                schema: "dbo",
                table: "Invoice",
                newName: "TablesTableID");

            migrationBuilder.RenameIndex(
                name: "IX_Invoice_TableID",
                schema: "dbo",
                table: "Invoice",
                newName: "IX_Invoice_TablesTableID");

            migrationBuilder.AddColumn<string>(
                name: "By",
                schema: "dbo",
                table: "Invoice",
                type: "varchar(125)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
