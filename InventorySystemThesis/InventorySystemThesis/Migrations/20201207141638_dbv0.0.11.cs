﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0011 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                column: "MenuItemTypeId",
                principalSchema: "dbo",
                principalTable: "MenuItemType",
                principalColumn: "MenuItemTypeId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AlterColumn<int>(
                name: "MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_MenuItemType_MenuItemTypeId",
                schema: "dbo",
                table: "MenuItem",
                column: "MenuItemTypeId",
                principalSchema: "dbo",
                principalTable: "MenuItemType",
                principalColumn: "MenuItemTypeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
