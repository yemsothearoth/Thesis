﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0029 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<int>(
                name: "StockId",
                schema: "dbo",
                table: "MenuItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Stock",
                schema: "dbo",
                columns: table => new
                {
                    StockId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StockNameEN = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    StockNameKH = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    StockQty = table.Column<double>(type: "float", nullable: false),
                    UoM = table.Column<string>(type: "nvarchar(125)", nullable: true),
                    CategoryCateID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stock", x => x.StockId);
                    table.ForeignKey(
                        name: "FK_Stock_Category_CategoryCateID",
                        column: x => x.CategoryCateID,
                        principalSchema: "dbo",
                        principalTable: "Category",
                        principalColumn: "CateID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StockId",
                schema: "dbo",
                table: "MenuItem",
                column: "StockId");

            migrationBuilder.CreateIndex(
                name: "IX_Stock_CategoryCateID",
                schema: "dbo",
                table: "Stock",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StockId",
                schema: "dbo",
                table: "MenuItem",
                column: "StockId",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StockId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StockId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropTable(
                name: "Stock",
                schema: "dbo");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StockId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StockId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.AddColumn<int>(
                name: "CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Category_CategoryCateID",
                schema: "dbo",
                table: "MenuItem",
                column: "CategoryCateID",
                principalSchema: "dbo",
                principalTable: "Category",
                principalColumn: "CateID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
