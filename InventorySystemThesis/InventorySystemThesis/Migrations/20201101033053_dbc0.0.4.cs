﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbc004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangedAmount",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "RecievedAmount",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<double>(
                name: "ChangedAmountKHR",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "ChangedAmountUSD",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "RecievedAmountKHR",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "RecievedAmountUSD",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangedAmountKHR",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "ChangedAmountUSD",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "RecievedAmountKHR",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "RecievedAmountUSD",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.AddColumn<double>(
                name: "ChangedAmount",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "RecievedAmount",
                schema: "dbo",
                table: "Invoice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
