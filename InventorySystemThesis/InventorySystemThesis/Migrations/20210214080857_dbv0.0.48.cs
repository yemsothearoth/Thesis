﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0048 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeAwaysInvoice_Tables_TableID",
                schema: "dbo",
                table: "TakeAwaysInvoice");

            migrationBuilder.RenameColumn(
                name: "TableID",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                newName: "TakeAwayId");

            migrationBuilder.RenameIndex(
                name: "IX_TakeAwaysInvoice_TableID",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                newName: "IX_TakeAwaysInvoice_TakeAwayId");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeAwaysInvoice_TakeAways_TakeAwayId",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                column: "TakeAwayId",
                principalSchema: "dbo",
                principalTable: "TakeAways",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TakeAwaysInvoice_TakeAways_TakeAwayId",
                schema: "dbo",
                table: "TakeAwaysInvoice");

            migrationBuilder.RenameColumn(
                name: "TakeAwayId",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                newName: "TableID");

            migrationBuilder.RenameIndex(
                name: "IX_TakeAwaysInvoice_TakeAwayId",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                newName: "IX_TakeAwaysInvoice_TableID");

            migrationBuilder.AddForeignKey(
                name: "FK_TakeAwaysInvoice_Tables_TableID",
                schema: "dbo",
                table: "TakeAwaysInvoice",
                column: "TableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
