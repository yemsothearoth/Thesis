﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0034 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UoM",
                schema: "dbo");

            migrationBuilder.DropColumn(
                name: "UoM",
                schema: "dbo",
                table: "Stock");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UoM",
                schema: "dbo",
                table: "Stock",
                type: "nvarchar(125)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UoM",
                schema: "dbo",
                columns: table => new
                {
                    UoMId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<string>(type: "nvarchar(5)", nullable: true),
                    UoMNameEn = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    UoMNameKH = table.Column<string>(type: "nvarchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UoM", x => x.UoMId);
                });
        }
    }
}
