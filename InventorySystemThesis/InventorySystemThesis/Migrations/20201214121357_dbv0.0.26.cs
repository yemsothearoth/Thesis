﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0026 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StocksID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StocksID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StocksID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "StocksID",
                schema: "dbo",
                table: "Stock",
                newName: "StockID");

            migrationBuilder.AddColumn<int>(
                name: "StockID",
                schema: "dbo",
                table: "MenuItem",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StockID",
                schema: "dbo",
                table: "MenuItem",
                column: "StockID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StockID",
                schema: "dbo",
                table: "MenuItem",
                column: "StockID",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StockID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropIndex(
                name: "IX_MenuItem_StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.DropColumn(
                name: "StockID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "StockID",
                schema: "dbo",
                table: "Stock",
                newName: "StocksID");

            migrationBuilder.AddColumn<int>(
                name: "StocksID",
                schema: "dbo",
                table: "MenuItem",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_StocksID",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StocksID",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksID",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StocksID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
