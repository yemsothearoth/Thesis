﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0013 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.AlterColumn<string>(
                name: "InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "InvoicesId",
                principalSchema: "dbo",
                principalTable: "Invoice",
                principalColumn: "InvoicesId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail");

            migrationBuilder.AlterColumn<string>(
                name: "InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_InvoiceDetail_Invoice_InvoicesId",
                schema: "dbo",
                table: "InvoiceDetail",
                column: "InvoicesId",
                principalSchema: "dbo",
                principalTable: "Invoice",
                principalColumn: "InvoicesId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
