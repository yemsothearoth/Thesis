﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0033 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "updateAt",
                schema: "dbo",
                table: "Stock",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "updateBy",
                schema: "dbo",
                table: "Stock",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "updateAt",
                schema: "dbo",
                table: "Stock");

            migrationBuilder.DropColumn(
                name: "updateBy",
                schema: "dbo",
                table: "Stock");
        }
    }
}
