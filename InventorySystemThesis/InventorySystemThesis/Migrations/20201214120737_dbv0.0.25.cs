﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0025 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StocksId",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "StocksId",
                schema: "dbo",
                table: "Stock",
                newName: "StocksID");

            migrationBuilder.RenameColumn(
                name: "StocksId",
                schema: "dbo",
                table: "MenuItem",
                newName: "StocksID");

            migrationBuilder.RenameIndex(
                name: "IX_MenuItem_StocksId",
                schema: "dbo",
                table: "MenuItem",
                newName: "IX_MenuItem_StocksID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StocksID",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksID",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StocksID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_Stock_StocksID",
                schema: "dbo",
                table: "MenuItem");

            migrationBuilder.RenameColumn(
                name: "StocksID",
                schema: "dbo",
                table: "Stock",
                newName: "StocksId");

            migrationBuilder.RenameColumn(
                name: "StocksID",
                schema: "dbo",
                table: "MenuItem",
                newName: "StocksId");

            migrationBuilder.RenameIndex(
                name: "IX_MenuItem_StocksID",
                schema: "dbo",
                table: "MenuItem",
                newName: "IX_MenuItem_StocksId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_Stock_StocksId",
                schema: "dbo",
                table: "MenuItem",
                column: "StocksId",
                principalSchema: "dbo",
                principalTable: "Stock",
                principalColumn: "StocksId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
