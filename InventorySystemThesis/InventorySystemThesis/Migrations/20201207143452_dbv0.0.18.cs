﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication2.Migrations
{
    public partial class dbv0018 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropIndex(
                name: "IX_Invoice_TablesTableID",
                schema: "dbo",
                table: "Invoice");

            migrationBuilder.DropColumn(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TablesTableID",
                schema: "dbo",
                table: "Invoice",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoice_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID");

            migrationBuilder.AddForeignKey(
                name: "FK_Invoice_Tables_TablesTableID",
                schema: "dbo",
                table: "Invoice",
                column: "TablesTableID",
                principalSchema: "dbo",
                principalTable: "Tables",
                principalColumn: "TableID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
