﻿using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using InventorySystemThesis.Repo.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.DepndencyInjection
{
    public static class DependencyInjection
    {

        public static void DI(IServiceCollection services)
        {
            services.AddScoped<IInvoicesRepo, InvoiceRepo>();
            services.AddScoped<IStocksRepo, StocksRepo>();
            services.AddScoped<IUsersRepo, UsersRepo>();
            services.AddScoped<ITableRepo, TableRepo>();
            services.AddScoped<ISysConfigsRepo, SysConfigsRepo>();
            services.AddScoped<ITakeAwaysRepo, TakeAwaysRepo>();
            services.AddScoped<ITAInvoicesRepo, TAInvoiceRepo>();
            services.AddScoped<ICategoryRepo, CategoryRepo>();
            services.AddScoped<IMenuItemRepo, MenuItemRepo>();


            services.TryAddScoped<RolesServices>();
            services.TryAddScoped<TableServices>();
            services.TryAddScoped<CategoryServices>();
            services.TryAddScoped<UoMServices>();
            services.TryAddScoped<StocksServices>();
            services.TryAddScoped<MenuItemsServices>();
            services.TryAddScoped<SystemConfigsServices>();
            services.TryAddScoped<TakeAwaysServices>();
        }
    }
}
