﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InventorySystemThesis.Models.Inventory.Invoices;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class InvoicesController : Controller
    {
        private readonly IInvoicesRepo _iinvoicesRepo;
        private readonly ITableRepo _itableRepo;

        public InvoicesController(IInvoicesRepo iinvoicesRepo, ITableRepo itableRepo)
        {
            _iinvoicesRepo = iinvoicesRepo;
            _itableRepo = itableRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> InvoicesList(DateTime from, DateTime to)
        {

            if (from == default(DateTime) || to == default(DateTime))
            {
                from = DateTime.Now;
                to = DateTime.Now;
                ViewBag.from = from.Date.ToString("yyyy-MM-dd");
                ViewBag.to = to.Date.ToString("yyyy-MM-dd");

            }
            else
            {
                ViewBag.from = from.Date.ToString("yyyy-MM-dd");
                ViewBag.to = to.Date.ToString("yyyy-MM-dd");

            }
            ViewBag.InvoiceList = await _iinvoicesRepo.InvoicesBydate(from, to);
            return View();

        }
        public IActionResult TableSale()
        {
            return View();

        }

        //[HttpPost]
        //public async Task<JsonResult> InvoicesList(DateTime from, DateTime to)
        //{
        //    if (from == default(DateTime) || to == default(DateTime))
        //    {
        //        from = DateTime.Now;
        //        to = DateTime.Now;
        //    }

        //    return new JsonResult(Ok(await _iinvoicesRepo.InvoicesBydate(from, to)));
        //}




        [HttpPost]
        public async Task<IActionResult> InvoicePrint(Invoices invoices)
        {
            invoices.updateBy=HttpContext.Session.GetString("username");

            return View(await _iinvoicesRepo.PaidInvoice(invoices));
        }

        [HttpGet]
        public async Task<IActionResult> InvoicePrint(string InvoicesId , string exchangeRate)
        {

            return View(await _iinvoicesRepo.FindById(InvoicesId));
        }


        //[HttpPost]
        //public async Task<IActionResult> VoidInv(string id)
        //{
        //    try
        //    {
        //        var invoiceitemvoid = await _iinvoicesRepo.FindById(id);
        //        invoiceitemvoid.Status = "Void";
        //        ViewBag.InvoiceList = await _iinvoicesRepo.UpdateInvoice(invoiceitemvoid);

        //        //input stock logic
        //        List<Product> salesupRange = new List<Product>();
        //        var salesInvList = _isalesRepo.FindByInvCode(invoiceitemvoid.InvoicesId);
        //        foreach (var item in salesInvList)
        //        {
        //            //query by productname in stock or query by productname and UoM
        //            Product productsRes = new Product(); //should be return from repo
        //            //after get response qty in stock + qty item.qty
        //            //productsRes.qty+=item.qty;
        //            salesupRange.Add(productsRes);

        //        }
        //        //insert list back to product using async

        //        // await iproductRepo.AddRange(salesupRange);

        //        //end stock logic

        //        //update table status logic

        //        //query table from using itableRepo by invoiceitemvoid.TableNo 

        //        //after get response update status 
        //        //update tbl status to != Y using async
        //        // await itableRepo.Update(tblRes)

        //        //end table status logic
        //        return RedirectToAction("InvoicesList");

        //    }
        //    catch (Exception)
        //    {
        //        return View();
        //    }
        //}

        //[HttpPost]
        //public async Task<IActionResult> UpdateInvoicePaid(Invoices invoices)
        //{
        //    try
        //    {
        //        var invoiceitem = await _iinvoicesRepo.FindById(invoices.InvoicesId);
        //        invoiceitem.Status = "Paid";
        //        invoiceitem.RecievedAmountUSD = invoices.RecievedAmountUSD;
        //        invoiceitem.ChangedAmountUSD = invoiceitem.TotalPaidUSD - invoices.RecievedAmountUSD;
        //        ViewBag.InvoiceList = await _iinvoicesRepo.UpdateInvoice(invoiceitem);
        //        return RedirectToAction("InvoicesList");
        //    }
        //    catch (Exception)
        //    {
        //        return View();
        //    }
        //}

        [HttpGet]
        public IActionResult InsertInvoice(int tblid,string tblname , int floor)
        {

            ViewBag.tblid = tblid;
            ViewBag.tblnumber = tblname;
            ViewBag.floor = floor;
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> InsertInvoice(Invoices invoices)
        {
            try
            {
                invoices.createBy = HttpContext.Session.GetString("username");
                await _iinvoicesRepo.AddeInvoices(invoices);
                if (invoices != null)
                {
                    await _itableRepo.UpdateUnavailable(invoices.TableID);
                    return new JsonResult(Ok(invoices));
                }

                else
                    return new JsonResult(BadRequest());
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());
            }
        }



        [HttpGet]
        public async Task<IActionResult> UpdateInvoice(int tblid, string tblname, int floor)
        {
            ViewBag.tblnumber = tblname;
            ViewBag.floor = floor;
            var inv = await _iinvoicesRepo.FindByTableId(tblid);
            return View(inv);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateInvoice(Invoices invoices)
        {
            try
            {
                invoices.updateBy = HttpContext.Session.GetString("username");
                var inv = await _iinvoicesRepo.UpdateInvoice(invoices);
                if (invoices != null)
                    return new JsonResult(Ok(inv));
                else
                    return new JsonResult(BadRequest());
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());
            }
        }


        [HttpPost]
        public async Task<JsonResult> PaidInvoice(Invoices invoices)
        {
            try
            {
                var ivn = await _iinvoicesRepo.PaidInvoice(invoices);
                return new JsonResult(Ok(ivn));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> VoidInvoice(string id)
        {
            try
            {
                return new JsonResult(Ok(await _iinvoicesRepo.VoidInvoice(id, HttpContext.Session.GetString("username"))));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> FindInvoiceById(string id)
        {
            try
            {
                return new JsonResult(await _iinvoicesRepo.FindById(id));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }


        [HttpPost]
        public async Task<JsonResult> validateUpdateQty(int invoiceDetailid, float newQty)
        {
            try
            {

                return new JsonResult(Ok(await _iinvoicesRepo.validateUpdateQty(invoiceDetailid, newQty)));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> validateInsertQty(int menuItemId, float Qty)
        {
            try
            {

                return new JsonResult(Ok(await _iinvoicesRepo.validateInsertQty(menuItemId, Qty)));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }


        //[HttpGet]
        //public IActionResult SalesInvCodeList(string invCode)
        //{

        //    var salesInvList =_isalesRepo.FindByInvCode(invCode);

        //    if (salesInvList == null)
        //    {
        //        salesInvList = new List<Sales> { new Sales { TableID = 1 } ,new Sales { TableID=2} };
        //    };
        //    ViewBag.salesInvList = salesInvList;
        //    return Ok(salesInvList);
        //    // return View();
        //}


    }
}