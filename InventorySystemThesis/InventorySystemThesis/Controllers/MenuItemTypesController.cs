﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InventorySystemThesis.Models.Inventory.Products;
using InventorySystemThesis.Models.Services;
using Microsoft.AspNetCore.Authorization;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class MenuItemTypesController : Controller
    {
        private readonly AppContexts _context;

        public MenuItemTypesController(AppContexts context)
        {
            _context = context;
        }

        // GET: MenuItemTypes
        public IActionResult MenuItemTypeList()

        {
            var menuItemtype = _context.MenuItemTypes.ToList();
            ViewBag.MenuItemTypes = menuItemtype;
            return View();
        }


        public ActionResult SaveMenuItemtype(MenuItemTypes menuItemtype)
        {


            _context.MenuItemTypes.Add(menuItemtype);
            _context.SaveChanges();

            return RedirectToAction("MenuItemTypeList");
        }


        public async Task<IActionResult> Editmenu(int? id)
        {
            var menuItemtype = await _context.MenuItemTypes.FindAsync(id);
            if (menuItemtype == null)
            {
                return NotFound();
            }
            return Ok(menuItemtype);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editmenu(MenuItemTypes menuItemtype)
        {
            var existingtbl = await _context.MenuItemTypes.FindAsync(menuItemtype.MenuItemTypeId);

            existingtbl.MenuItemTypeName = menuItemtype.MenuItemTypeName;


            _context.MenuItemTypes.Update(existingtbl);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

                throw e;
            }

            return RedirectToAction(nameof(MenuItemTypeList));
        }

        [HttpPost]
        public async Task<JsonResult> Remove(int id)
        {
            bool status = false;

            var menuItemtype = await _context.MenuItemTypes.FindAsync(id);
            _context.MenuItemTypes.Remove(menuItemtype);
            try
            {
                await _context.SaveChangesAsync();
                status = true;
            }
            catch (Exception)
            {

                throw;
            }

            return new JsonResult(new { data = status });

        }
    }
}
