﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventorySystemThesis.Models.Inventory.Table;
using InventorySystemThesis.Models.Services;
using Microsoft.AspNetCore.Authorization;
using InventorySystemThesis.Repo.IRepository;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class TablesController : Controller
    {
        
        private readonly ITableRepo _itableRepo;
        public TablesController(ITableRepo itableRepo)
        {
            _itableRepo = itableRepo;
        }

        // GET: Tables
        public IActionResult table()
        {
            var tables = _itableRepo.TableList();
            ViewBag.Tables = tables;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTable(Tables tables)
        {
            tables.Status = "Y";

            _itableRepo.SaveTable(tables);
            return RedirectToAction("table");
        }




        // GET: Tables/Edit/5

        public IActionResult EditTable(int id)
        {
            var tables = _itableRepo.FindTableById(id);
            if (tables == null)
            {
                return NotFound();
            }
            return Ok(tables);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditTable(Tables tables)
        {
            _itableRepo.UpdateTable(tables);
            return RedirectToAction(nameof(table));
        }




        // GET: Tables/Delete/5
        /*public async Task<IActionResult> Remove(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tables = await _context.Tables
                .FirstOrDefaultAsync(m => m.TableID == id);
            if (tables == null)
            {
                return NotFound();
            }

            return View(tables);
        }*/

        // POST: Tables/Delete/5
        [HttpPost]
        public JsonResult DeleteTable(int id)
        {
            bool status = _itableRepo.DeleteTable(id);

            return new JsonResult(new { data = status });
        }


        /*
                private bool TablesExists(int id)
                {
                    return _context.Tables.Any(e => e.TableID == id);
                }
        */


    }
}
