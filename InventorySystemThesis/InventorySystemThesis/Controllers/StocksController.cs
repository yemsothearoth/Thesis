﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventorySystemThesis.Models.Inventory.Stocks;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class StocksController : Controller
    {
        private readonly IStocksRepo _istockRepo;
        private readonly IUsersRepo _iusersRepo;

        public StocksController(IStocksRepo istockRepo, IUsersRepo iusersRepo)
        {
            _iusersRepo = iusersRepo;
            _istockRepo = istockRepo;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> StocksList()
        {
            if (!checkPermssion())
            {
                await _iusersRepo.LogoutUser(int.Parse(HttpContext.Session.GetString("userid")));
                HttpContext.Session.Clear();
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("forbiddenpage","Home");
            }
            ViewBag.StocksList = await _istockRepo.StocksList();
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateStock(Stock stock)
        {
            stock.updateBy = HttpContext.Session.GetString("username");
            await _istockRepo.createNewStock(stock);
            return RedirectToAction("StocksList", "Stocks");
        }
        [HttpPost]
        public async Task<IActionResult> UpdateStock(Stock stock)
        {
            stock.updateBy = HttpContext.Session.GetString("username");
            await _istockRepo.updateStock(stock);
            return RedirectToAction("StocksList", "Stocks");
        }
        [HttpPost]
        public async Task<JsonResult> DeleteStock(int id)
        {
            await _istockRepo.deleteStock(id);
            return new JsonResult(new { data =await _istockRepo.deleteStock(id) });
        }
        [HttpGet]
        public async Task<IActionResult> FindById(int id)
        {
            var stockList = await _istockRepo.FindById(id);
            return Ok(stockList);
        }

        private bool checkPermssion() {
            if (HttpContext.Session.GetString("rolename").Trim() == "Admin")
                return true;
            else
                return false;
        }
    }
}