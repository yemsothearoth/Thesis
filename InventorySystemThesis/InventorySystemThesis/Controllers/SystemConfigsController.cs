using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventorySystemThesis.Models.Inventory.SystemConfigs;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystemThesis.Controllers
{

    [Authorize]
    public class SystemConfigsController : Controller
    {
        private readonly ISysConfigsRepo _sysConfigsRepo;
        public SystemConfigsController(ISysConfigsRepo sysConfigsRepo) 
        {
            _sysConfigsRepo = sysConfigsRepo;
        }

        [HttpGet]
        public IActionResult SystemConfigsList() => View();
      

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSystemConfigs(SystemConfig systemConfig)
        {
            await _sysConfigsRepo.AddSystemConfig(systemConfig);
            return Redirect(nameof(SystemConfigsList));
        }

        [HttpPost]
        public async Task<IActionResult> UpdateSystemConfigs(SystemConfig systemConfig)
        {
            //return new JsonResult(new {data= await _sysConfigsRepo.UpdateSystemConfig(systemConfig) });
            await _sysConfigsRepo.UpdateSystemConfig(systemConfig);
            return Redirect(nameof(SystemConfigsList));
        }

        [HttpPost]
        public async Task<JsonResult> DeleteSystemConfigs(int id)
        {
            return new JsonResult(new { data = await _sysConfigsRepo.DeleteSystemConfig(id) });
        }

        [HttpGet]
        public async Task<IActionResult> GetSysConfigById(int id)
        {
            return Ok(await _sysConfigsRepo.GetSystemConfigById(id));
        }

        [HttpGet]
        public async Task<IActionResult> GetSysConfigByKey(string key)
        {
            return Ok(await _sysConfigsRepo.GetSystemConfigByKey(key));
        }
    }
}