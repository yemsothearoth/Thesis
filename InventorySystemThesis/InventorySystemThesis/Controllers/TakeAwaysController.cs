﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventorySystemThesis.Models.Inventory.Table;
using InventorySystemThesis.Models.Services;
using Microsoft.AspNetCore.Authorization;
using InventorySystemThesis.Models.Inventory.TakeAways;
using InventorySystemThesis.Repo.IRepository;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class TakeAwaysController : Controller
    {
        
        private readonly ITakeAwaysRepo _itakeAwaysRepo;
        public TakeAwaysController(ITakeAwaysRepo itakeAwaysRepo)
        {
            _itakeAwaysRepo = itakeAwaysRepo;
        }

        // GET: Tables
        public IActionResult TakeAway()
        {
            var ta = _itakeAwaysRepo.TakeAwayList();
            ViewBag.TakeAway = ta;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveTakeAways(TakeAway takeAway)
        {
            takeAway.Status = "Y";

            _itakeAwaysRepo.SaveTakeAway(takeAway);
            return RedirectToAction(nameof(TakeAway));
        }




        // GET: Tables/Edit/5

        public IActionResult TakeAways(int id)
        {
            var takeaways = _itakeAwaysRepo.FindTaById(id);
            if (takeaways == null)
            {
                return NotFound();
            }
            return Ok(takeaways);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditTakeAways(TakeAway takeAway)
        {
            _itakeAwaysRepo.UpdateTa(takeAway);
            return RedirectToAction(nameof(takeAway));

        }

        [HttpPost]
        public JsonResult DeleteTakeAways(int id)
        {
            bool status = _itakeAwaysRepo.DeleteTa(id);

            return new JsonResult(new { data = status });
        }
        /*  [HttpPost]
          public async Task<JsonResult> DeleteTakeAways(int id)
          {
              var ta = await _context.TakeAways.FindAsync(id);
              bool status = true;
              _context.TakeAways.Remove(ta);
              try
              {
                  await _context.SaveChangesAsync();

              }
              catch (Exception )
              {
                  status = false;

              }
              await _context.SaveChangesAsync();
              return new JsonResult(new { data = status });

          }*/
    }
}