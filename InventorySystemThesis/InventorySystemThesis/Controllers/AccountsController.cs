﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using InventorySystemThesis.Models.Inventory.Users;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class AccountsController : Controller
    {
        private readonly IUsersRepo _iUsersRepo;


        public AccountsController(IUsersRepo iUsersRepo)
        {
            _iUsersRepo = iUsersRepo;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> FindByUsername(string username)
        {
            return new JsonResult(Ok(await _iUsersRepo.FindByUsername(username)));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }
        [HttpGet]

        public async Task<IActionResult> Logout()
        {
            await _iUsersRepo.LogoutUser(int.Parse(HttpContext.Session.GetString("userid")));
            HttpContext.Session.Clear();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login","Accounts");
        }
        [HttpGet]
        public IActionResult Dashboard() => View();
        [HttpGet]
        public IActionResult Register() => View();

        
        [HttpGet]

        public async Task<IActionResult> Users()
        {
            if (!checkPermssion()) {
                await _iUsersRepo.LogoutUser(int.Parse(HttpContext.Session.GetString("userid")));
                HttpContext.Session.Clear();
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("forbiddenpage", "Home");
            }
            ViewBag.Users = await _iUsersRepo.UserList();
            return View();
            
        }


        [HttpPost]
        public async Task<IActionResult> Add(Users users)
        {
            await _iUsersRepo.InsertUser(users);
            return RedirectToAction("Users","Accounts");
         
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser(Users users)
        {
            await _iUsersRepo.UpdateUser(users);
            return RedirectToAction("Users", "Accounts");

        }
        [HttpPost]
        public async Task<JsonResult> DeleteUser(int id)
        {
            //await _iUsersRepo.DeleteUser(users);
            // return RedirectToAction("Users", "Accounts");
            return new JsonResult(new {
                data = await _iUsersRepo.DeleteUser(id)
            });

        }

        [HttpGet]
        public async Task<IActionResult> FindUserById(int id)
        {
            return Ok(await _iUsersRepo.FindById(id));

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(Users users)
        {
            if (!ModelState.IsValid) { return View(); }
            
            var UsersResp = await _iUsersRepo.LoginUsers(users);
            //UsersResp=true
            if (UsersResp != null)
            {
                var claims = new List<Claim> { new Claim(ClaimTypes.Name, UsersResp.Username) };
                //var roles = usr.UserRole.RoleEn.Split(",");
                //claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var principal = new ClaimsPrincipal(identity);
                var props = new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddMinutes(30)
                };
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props).Wait();
                HttpContext.Session.SetString("username", UsersResp.Username);
                HttpContext.Session.SetString("userid", UsersResp.UserID.ToString());
                HttpContext.Session.SetString("rolename", UsersResp.Roles.RolesName);
                return RedirectToAction("Dashboard", "Accounts");
            }

            else
            {
                ViewBag.ngounse = "Username or Password is incorrect";
                return View();
               
            }
                

        }

        private bool checkPermssion()
        {
            if (HttpContext.Session.GetString("rolename").Trim() == "Admin")
                return true;
            else
                return false;
        }

    }
}