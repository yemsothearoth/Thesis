﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InventorySystemThesis.Models.Inventory.Products;
using InventorySystemThesis.Models.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using InventorySystemThesis.Repo.IRepository;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class MenuItemsController : Controller
    {
        private readonly IMenuItemRepo _imenuItemRepo;

        public MenuItemsController(IMenuItemRepo imenuItemRepo)
        {
            _imenuItemRepo = imenuItemRepo;
        }
        public IActionResult MenuItemsList()
        {
            var menuItem = _imenuItemRepo.MenuItemList();
            ViewBag.MenuItems = menuItem;
            return View();
        }

        [HttpPost]
        public IActionResult CreateMenuItem(MenuItem menuItem)
        {



           _imenuItemRepo.SaveMenuItem(menuItem);
            return RedirectToAction("MenuItemsList");

         
        }

        [HttpGet]
        public IActionResult UpdateMenuItem(int id)
        {
            var menuItem = _imenuItemRepo.FindMenuItemById(id);
            return Ok(menuItem);
        }

        [HttpGet]
        public IActionResult FindById(int id)
        {
            var menuItem = _imenuItemRepo.FindMenuItemById(id);
            return Ok(menuItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult   UpdateMenuItem(MenuItem menuItem)
        {
            _imenuItemRepo.UpdateMenuItem(menuItem);

            return RedirectToAction(nameof(MenuItemsList));
        }

        // [HttpPost]
        /*public async Task<JsonResult> Remove(int id)
        {
            bool status = false;

            var menuItem = await imenuItemRepo.MenuItems.FindAsync(id);
           imenuItemRepo.MenuItems.Remove(menuItem);
            try
            {
             await imenuItemRepo.SaveChangesAsync();
                status = true;
            }
            catch (Exception)
            {

                throw;
            }

            return new JsonResult(new { data = status});

        }*/

        [HttpPost]
        public JsonResult DeletemenuItem(int id)
        {
            bool status = _imenuItemRepo.DelateMenuItem(id);

            return new JsonResult(new { data = status });
        }

    }
}
