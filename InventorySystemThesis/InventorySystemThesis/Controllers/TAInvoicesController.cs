﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InventorySystemThesis.Models.Inventory.Invoices;
using InventorySystemThesis.Models.Inventory.TakeAwaysInvoices;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class TAInvoicesController : Controller
    {
        private readonly ITAInvoicesRepo _iinvoicesRepo;
        private readonly ITakeAwaysRepo _itakeawaysRepo;

        public TAInvoicesController(ITAInvoicesRepo iinvoicesRepo, ITakeAwaysRepo itakeawaysRepo)
        {
            _iinvoicesRepo = iinvoicesRepo;
            _itakeawaysRepo = itakeawaysRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> InvoicesList(DateTime from, DateTime to)
        {

            if (from == default(DateTime) || to == default(DateTime))
            {
                from = DateTime.Now;
                to = DateTime.Now;
                ViewBag.from = from.Date.ToString("yyyy-MM-dd");
                ViewBag.to = to.Date.ToString("yyyy-MM-dd");

            }
            else
            {
                ViewBag.from = from.Date.ToString("yyyy-MM-dd");
                ViewBag.to = to.Date.ToString("yyyy-MM-dd");

            }
            ViewBag.InvoiceList = await _iinvoicesRepo.InvoicesBydate(from, to);
            return View();

        }
        public IActionResult TASale()
        {
            return View();

        }


        [HttpPost]
        public async Task<IActionResult> InvoicePrint(TAInvoices invoices)
        {
            invoices.updateBy=HttpContext.Session.GetString("username");

            return View(await _iinvoicesRepo.PaidInvoice(invoices));
        }

        [HttpGet]
        public async Task<IActionResult> InvoicePrint(string InvoicesId , string exchangeRate)
        {

            return View(await _iinvoicesRepo.FindById(InvoicesId));
        }


        [HttpGet]
        public IActionResult InsertInvoice(int taid,string name)
        {

            ViewBag.taid = taid;
            ViewBag.taname = name;
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> InsertInvoice(TAInvoices invoices)
        {
            try
            {
                invoices.createBy = HttpContext.Session.GetString("username");
                await _iinvoicesRepo.AddeInvoices(invoices);
                if (invoices != null)
                {
                    await _itakeawaysRepo.UpdateUnavailable(invoices.TakeAwayId);
                    return new JsonResult(Ok(invoices));
                }

                else
                    return new JsonResult(BadRequest());
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());
            }
        }



        [HttpGet]
        public async Task<IActionResult> UpdateInvoice(int taid, string name)
        {
            ViewBag.name = name;
            var inv = await _iinvoicesRepo.FindByTakeAwaysId(taid);
            return View(inv);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateInvoice(TAInvoices invoices)
        {
            try
            {
                invoices.updateBy = HttpContext.Session.GetString("username");
                var inv = await _iinvoicesRepo.UpdateInvoice(invoices);
                if (invoices != null)
                    return new JsonResult(Ok(inv));
                else
                    return new JsonResult(BadRequest());
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());
            }
        }


        [HttpPost]
        public async Task<JsonResult> PaidInvoice(TAInvoices invoices)
        {
            try
            {
                var ivn = await _iinvoicesRepo.PaidInvoice(invoices);
                return new JsonResult(Ok(ivn));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> VoidInvoice(string id)
        {
            try
            {
                return new JsonResult(Ok(await _iinvoicesRepo.VoidInvoice(id, HttpContext.Session.GetString("username"))));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> FindInvoiceById(string id)
        {
            try
            {
                return new JsonResult(await _iinvoicesRepo.FindById(id));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }


        [HttpPost]
        public async Task<JsonResult> validateUpdateQty(int invoiceDetailid, float newQty)
        {
            try
            {

                return new JsonResult(Ok(await _iinvoicesRepo.validateUpdateQty(invoiceDetailid, newQty)));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }

        [HttpPost]
        public async Task<JsonResult> validateInsertQty(int menuItemId, float Qty)
        {
            try
            {

                return new JsonResult(Ok(await _iinvoicesRepo.validateInsertQty(menuItemId, Qty)));
            }
            catch (Exception)
            {
                return new JsonResult(BadRequest());

            }
        }


    }
}