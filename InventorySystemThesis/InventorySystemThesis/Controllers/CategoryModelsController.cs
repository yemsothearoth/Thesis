﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Models.Inventory.Categories;
using Microsoft.AspNetCore.Authorization;
using InventorySystemThesis.Repo.IRepository;

namespace InventorySystemThesis.Controllers
{
    [Authorize]
    public class CategoryModelsController : Controller
    {
        
        private readonly ICategoryRepo _icategoryRepo;
        public CategoryModelsController(ICategoryRepo icategoryRepo)
        {
            _icategoryRepo = icategoryRepo;
        }

        // GET: CategoryModels
        public IActionResult CategoryList()
        {
            var category = _icategoryRepo.CategoryList();
            ViewBag.CategoryModels = category;
            return View();
        }


        public ActionResult SaveCategory(Category category)
        {
            category.Status = "S";

            _icategoryRepo.SaveCategory(category);
            return RedirectToAction("CategoryList");
        }

        [HttpGet]
        public IActionResult EditCategory(int id)
        {
            var category = _icategoryRepo.FindCateById(id);
            if (category == null)
            {
                return NotFound();
            }
            return Ok(category);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditCategory(Category category)
        {
            _icategoryRepo.UpdateCate(category);

            return RedirectToAction(nameof(CategoryList));
        }

        [HttpPost]
        public JsonResult DeleteCategory(int id)
        {
            bool status = _icategoryRepo.DeleteCate(id);

            return new JsonResult(new { data = status });
        }

        /*public async Task<JsonResult> Remove(int id)
        {
            var category = await _context.Category.FindAsync(id);
            bool Status = true;
            _context.Category.Remove(category);
            try
            {
                await _context.SaveChangesAsync();
               }
              catch (Exception)
            {
                Status = false;
            }

            await _context.SaveChangesAsync();
            return new JsonResult(new { data = Status });

        }
        private bool CategoryModelExists(int id)
        {
            return _context.Category.Any(e => e.CateID == id);
        }*/
    }
}
