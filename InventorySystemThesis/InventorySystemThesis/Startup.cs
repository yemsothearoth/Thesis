using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.SeedData;
using InventorySystemThesis.DepndencyInjection;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using InventorySystemThesis.Repo.Repository;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace InventorySystemThesis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppContexts>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            //DependencyInjection.DI(services);
            services.AddScoped<IInvoicesRepo, InvoiceRepo>();
            services.AddScoped<IStocksRepo, StocksRepo>();
            services.AddScoped<IUsersRepo, UsersRepo>();
            services.AddScoped<ITableRepo, TableRepo>();
            services.AddScoped<ISysConfigsRepo, SysConfigsRepo>();
            services.AddScoped<ITakeAwaysRepo, TakeAwaysRepo>();
            services.AddScoped<ITAInvoicesRepo, TAInvoiceRepo>();
            services.AddScoped<ICategoryRepo, CategoryRepo>();
            services.AddScoped<IMenuItemRepo, MenuItemRepo>();


            services.TryAddScoped<RolesServices>();
            services.TryAddScoped<TableServices>();
            services.TryAddScoped<CategoryServices>();
            services.TryAddScoped<UoMServices>();
            services.TryAddScoped<StocksServices>();
            services.TryAddScoped<MenuItemsServices>();
            services.TryAddScoped<SystemConfigsServices>();
            services.TryAddScoped<TakeAwaysServices>();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddSession(options =>
               {
                   options.IdleTimeout = TimeSpan.FromHours(1);
                   options.Cookie.HttpOnly = true;
               });
            services.AddAuthorization();
            //add service cookice
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(Opt =>
            {
                Opt.AccessDeniedPath = "/Accounts/Login";
                Opt.LoginPath = "/Accounts/Login";
            });
            services.AddDistributedMemoryCache();
            services.AddRouting(options=>options.LowercaseUrls=true);
            services.AddResponseCaching();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.UseHsts();
            }
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            

            app.UseRouting();
            app.UseSession();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Accounts}/{action=Login}/{id?}");
            });
            PrepData.PrepPopulation(app);
        }
    }
}
