using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace InventorySystemThesis.Helpers
{
    public class CheckAuthorization:AuthorizeAttribute,IAuthorizationFilter
    {
        public  void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User.Identity.IsAuthenticated;
            var userId = context.HttpContext.Session.GetString("usrId");
            if (!user)
            {
                return;
            } 
            if (userId == null)
            {
                context.Result = new RedirectResult("/accounts/login");
            }
        }
    }
}