using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace InventorySystemThesis.Helpers
{
    public class CheckRole:AuthorizeAttribute,IAuthorizationFilter
    {

        public async void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User.Identity.IsAuthenticated;
            var rolename = context.HttpContext.Session.GetString("rolename");
            if (!user)
            {
                return;
            } 
            if (rolename.ToLower().Trim() != "admin")
            {
                var userRepo = context.HttpContext.RequestServices.GetService(typeof(IUsersRepo)) as IUsersRepo;
                await userRepo.LogoutUser(int.Parse(context.HttpContext.Session.GetString("userid")));
                context.HttpContext.Session.Clear();
                await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                //  context.Result =// new RedirectResult("/home/forbiddenpage");
                context.Result = new RedirectToRouteResult(
                  new RouteValueDictionary
                  {
                     { "controller", "Home" },
                     { "action", "forbiddenpage" }
                  });

            }
        }
    }
}