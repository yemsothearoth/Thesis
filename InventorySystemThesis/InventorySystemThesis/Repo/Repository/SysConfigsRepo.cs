﻿using InventorySystemThesis.Models.Inventory.SystemConfigs;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class SysConfigsRepo : ISysConfigsRepo
    {
        private readonly AppContexts _contexts;
        public SysConfigsRepo(AppContexts contexts)
        { 
            _contexts=contexts;
        }
        public async Task<bool> AddSystemConfig(SystemConfig systemConfig)
        {
            await _contexts.SystemConfigs.AddAsync(systemConfig);
            try
            {
               return await _contexts.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> DeleteSystemConfig(int id)
        {
            var sysConfigRes = await _contexts.SystemConfigs.FindAsync(id);
            _contexts.SystemConfigs.Remove(sysConfigRes);

            try
            {
                return await _contexts.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<SystemConfig> GetSystemConfigById(int id)
        {
            return await _contexts.SystemConfigs.Where(_ => _.Id == id).FirstOrDefaultAsync();
        }

        public async Task<SystemConfig> GetSystemConfigByKey(string key)
        {
            return await _contexts.SystemConfigs.Where(_ => _.Key == key).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<SystemConfig>> SystemConfigList()
        {
            return await _contexts.SystemConfigs.ToListAsync();
        }

        public async Task<bool> UpdateSystemConfig(SystemConfig systemConfig)
        {
            var sysConfigRes = await _contexts.SystemConfigs.FindAsync(systemConfig.Id);

            //sysConfigRes.Key = systemConfig.Key;
            sysConfigRes.Value = systemConfig.Value;
             _contexts.SystemConfigs.Update(sysConfigRes);

            try
            {
                return await _contexts.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
