﻿using InventorySystemThesis.Models.Inventory.Invoices;
using InventorySystemThesis.Models.Inventory.TakeAwaysInvoices;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class TAInvoiceRepo : ITAInvoicesRepo
    {
        private readonly AppContexts _contexts;
        public TAInvoiceRepo(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<TAInvoices> AddeInvoices(TAInvoices invoices)
        {
            try
            {


                foreach (var item in invoices.InvoicesDetail)
                {
                    var menu = await _contexts.MenuItems.Where(_ => _.MenuItemId == item.MenuItemId)
                        .Include(_=>_.Stock)
                        .FirstOrDefaultAsync();

                    var stock = await _contexts.Stocks.Where(_ => _.StockId == menu.StockId).FirstOrDefaultAsync();
                    stock.StockQty -= (menu.qtytoStock * item.Qty);
                    stock.updateAt = DateTime.Now.ToLocalTime();
                    stock.updateBy = invoices.createBy;
                    _contexts.Stocks.Update(stock);
                    await _contexts.SaveChangesAsync();

                }
                invoices.InvoicesId = GenerateInvcode();
                invoices.createAt = DateTime.Now.ToLocalTime();
                await _contexts.TAInvoices.AddAsync(invoices);
                await _contexts.SaveChangesAsync();
                return invoices;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<TAInvoices> FindById(string id)
        {
            try
            {
                return await _contexts.TAInvoices
                    .Include(_ => _.TakeAway)
                    .Include(_=>_.InvoicesDetail)
                    .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.Category)
                    .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                    .Where(_ => _.InvoicesId == id)
                    .FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<TAInvoices> FindByTakeAwaysId(int taid)
        {
            return await _contexts.TAInvoices
                   .Include(_ => _.TakeAway)
                   .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.Category)
                   .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                   .Where(_ => _.TakeAwayId == taid && _.Status == null)
                   .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TAInvoices>> InvoicesBydate(DateTime from, DateTime to)
        {
            try
            {
                return await _contexts.TAInvoices
               .Include(_ => _.TakeAway)
               .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
               .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.Category)
               .Where(_ => _.createAt.Date >= from.Date && _.createAt.Date <= to.Date)
               .OrderBy(_ => _.createAt)
               .ToListAsync();
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public async Task<IEnumerable<TAInvoices>> InvoicesList()
        {
            try
            {
                return await _contexts.TAInvoices
                    .Include(_ => _.TakeAway)
                    .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                    .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.Category)
                    .ToListAsync();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<TAInvoices> PaidInvoice(TAInvoices invoices)
        {
            var invoiceitem = await _contexts.TAInvoices
                     .Include(_ => _.TakeAway)
                     .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                     .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock).ThenInclude(_ => _.Category)
                     .Where(_ => _.InvoicesId == invoices.InvoicesId)
                     .FirstOrDefaultAsync();

            var config = await _contexts.SystemConfigs.Where(_ => _.Key == "EXCHANGE_RATE").FirstOrDefaultAsync();
            var exchangeRate = float.Parse(config.Value);
            invoiceitem.Status = "Paid";
            invoiceitem.RecievedAmountUSD = invoices.RecievedAmountUSD;
            invoiceitem.RecievedAmountKHR = invoices.RecievedAmountKHR;
            invoiceitem.updateBy = invoices.updateBy;
            invoiceitem.updateAt = DateTime.Now.ToLocalTime();

            // convert recieved USD to KHR
            var convertRecievedUSDtoKHR = invoices.RecievedAmountUSD * exchangeRate;

            // Total of recieved Both USD and KHR (total as KHR)
            var totalRecieved = invoices.RecievedAmountKHR + convertRecievedUSDtoKHR;

            // Total of Change money after bill in KHR
            var totalChanged = totalRecieved - invoiceitem.TotalPaidKHR;

            // Convert Total Change to USD and pass it to invoiceItems
            invoiceitem.ChangedAmountKHR = totalChanged;
            invoiceitem.ChangedAmountUSD = totalChanged / exchangeRate;
   
            //if(invoiceitem.TotalPaidUSD < invoices.RecievedAmountUSD)
            //    invoiceitem.ChangedAmountUSD = invoices.RecievedAmountUSD - invoiceitem.TotalPaidUSD ;

            //if (invoiceitem.TotalPaidKHR < invoices.RecievedAmountKHR)
            //    invoiceitem.ChangedAmountKHR = invoiceitem.TotalPaidKHR - invoices.RecievedAmountKHR;

            _contexts.TAInvoices.Update(invoiceitem);

            var ta = await _contexts.TakeAways.Where(_ => _.Id == invoiceitem.TakeAwayId).FirstOrDefaultAsync();
            ta.Status = "Y";
            _contexts.TakeAways.Update(ta);

            try
            {
                await _contexts.SaveChangesAsync();
                return invoiceitem;
            }
            catch (Exception e)
            {

                throw e;
            }


        }

        public async Task<TAInvoices> UpdateInvoice(TAInvoices invoices)
        {
            try
            {
                var inv = await _contexts.TAInvoices
                     .Include(_ => _.TakeAway)
                     .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem)
                     .Where(_ => _.InvoicesId == invoices.InvoicesId).FirstOrDefaultAsync();


                for (int i = 0; i < inv.InvoicesDetail.Count; i++)
                {
                    var stock = await _contexts.Stocks
                        .Include(_ => _.Category)
                        .Include(_ => _.UnitOfMeasure)
                        .Where(_ => _.StockId == inv.InvoicesDetail[i].MenuItem.StockId).FirstOrDefaultAsync();

                    stock.StockQty += (inv.InvoicesDetail[i].MenuItem.qtytoStock * inv.InvoicesDetail[i].Qty);
                    _contexts.Stocks.Update(stock);
                    try
                    {
                        await _contexts.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {

                        throw e;
                    }
                }

                //take out stokc
                for (int i = 0; i < invoices.InvoicesDetail.Count; i++)
                {
                    //query menuitem to get stock detail 
                    var menuitem = await _contexts.MenuItems
                         .Include(_ => _.Stock)
                         .Where(_ => _.MenuItemId == invoices.InvoicesDetail[i].MenuItemId)
                         .FirstOrDefaultAsync();

            
                    menuitem.Stock.StockQty -= (menuitem.qtytoStock * invoices.InvoicesDetail[i].Qty);
                    menuitem.Stock.updateAt = DateTime.Now.ToLocalTime();
                    menuitem.Stock.updateBy = invoices.updateBy;
                    _contexts.Stocks.Update(menuitem.Stock);
                    try
                    {
                        await _contexts.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {

                        throw e;
                    }
                }
                //end


                //remove existing detail
                _contexts.TAInvoicesDetails.RemoveRange(inv.InvoicesDetail);

                //add new detail
                inv.InvoicesDetail = invoices.InvoicesDetail;
                //update inv
                inv.updateAt = DateTime.Now.ToLocalTime();
                inv.updateBy = invoices.updateBy;
                inv.TotalPaidUSD = invoices.TotalPaidUSD;
                inv.TotalPaidKHR = invoices.TotalPaidKHR;
                inv.CustomerName = invoices.CustomerName;
                inv.PhoneNumber = invoices.PhoneNumber;
                inv.Address = invoices.Address;
                inv.Discount = invoices.Discount;

                _contexts.TAInvoices.Update(inv);
                await _contexts.SaveChangesAsync();
                return invoices;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<bool> validateInsertQty(int menuItemId, float Qty)
        {
            var menuItem = await _contexts.MenuItems
                .Where(_ => _.MenuItemId == menuItemId)
                .Include(_ => _.Stock)
                .FirstOrDefaultAsync();

            var takeOutQty = menuItem.qtytoStock * Qty;

            if (menuItem.Stock.StockQty < takeOutQty)
                return false;
            else
                return true;

        }

        public async Task<bool> validateUpdateQty(int invoiceDetailId, float newQty)
        {
            var invDetail = await _contexts.TAInvoicesDetails
                .Include(_ => _.MenuItem)
                .Where(_ => _.InvoicesDetailId == invoiceDetailId).FirstOrDefaultAsync();
            float oldQty = invDetail.MenuItem.qtytoStock * invDetail.Qty;
            newQty = invDetail.MenuItem.qtytoStock * newQty;

            //qty increase
            if (newQty > oldQty)
            {
                var stock = await _contexts.Stocks.Where(_ => _.StockId == invDetail.MenuItem.StockId).FirstOrDefaultAsync();
                newQty -= oldQty;
                if (stock.StockQty > newQty)
                    return true;
                else
                    return false;
            }

            //qty decrease
            return true;
        }

        public async Task<bool> VoidInvoice(string id, string username)
        {
            var inv = await _contexts.TAInvoices
                .Include(_ => _.InvoicesDetail).ThenInclude(_ => _.MenuItem).ThenInclude(_ => _.Stock)
                .Include(_ => _.TakeAway)
                .Where(_ => _.InvoicesId == id).FirstOrDefaultAsync();

            inv.Status = "Void";
            inv.updateAt = DateTime.Now.ToLocalTime();
            inv.updateBy = username;

            for (int i = 0; i < inv.InvoicesDetail.Count; i++)
            {
                var stock = await _contexts.Stocks
                    .Include(_ => _.Category)
                    .Include(_ => _.UnitOfMeasure)
                    .Where(_ => _.StockId == inv.InvoicesDetail[i].MenuItem.StockId).FirstOrDefaultAsync();

                stock.StockQty += (inv.InvoicesDetail[i].MenuItem.qtytoStock * inv.InvoicesDetail[i].Qty);
                stock.updateAt = DateTime.Now.ToLocalTime();
                stock.updateBy = username;
                _contexts.Stocks.Update(stock);
                try
                {
                    await _contexts.SaveChangesAsync();
                }
                catch (Exception e)
                {

                    throw e;
                }
            }
            //contexts.InvoicesDetails.RemoveRange(inv.InvoicesDetail);
            _contexts.TAInvoices.Update(inv);

            var ta = await _contexts.TakeAways.FindAsync(inv.TakeAwayId);
            ta.Status = "Y";
            _contexts.TakeAways.Update(ta);

            try
            {
                return await _contexts.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private string GenerateInvcode()
        {
            var invprefix = _contexts.SystemConfigs.Where(_ => _.Key == "TA_INV_PREFIX").FirstOrDefault();
            return
                invprefix.Value + DateTime.UtcNow.Date.Year.ToString() +
                DateTime.UtcNow.Date.Month.ToString() +
                DateTime.UtcNow.Date.Day.ToString() + Guid.NewGuid().ToString().Substring(0, 4).ToUpper();
        }
    }
}
