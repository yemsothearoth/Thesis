﻿using InventorySystemThesis.Models.Inventory.Products;
using InventorySystemThesis.Models.Inventory.Stocks;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class StocksRepo : IStocksRepo
    {
        private readonly AppContexts _contexts;
        public StocksRepo(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<bool> createNewStock(Stock stock)
        {
            try
            {
                stock.updateAt = DateTime.Now.ToLocalTime();
                stock.Status = "Y";
                await _contexts.Stocks.AddAsync(stock);
                await _contexts.SaveChangesAsync();

                return true;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public async Task<bool> deleteStock(int id)
        {
            try
            {
                var existingStock = await _contexts.Stocks.Include(_=>_.Category).Where(p => p.StockId == id).FirstOrDefaultAsync();
                existingStock.Status = "N";
                _contexts.Stocks.Update(existingStock);
                await _contexts.SaveChangesAsync();
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<bool> updateStock(Stock stock)
        {
            try
            {
                var existingStock = await _contexts.Stocks.Where(p => p.StockId == stock.StockId).Include(_=>_.Category).Include(_=>_.UnitOfMeasure).FirstOrDefaultAsync();
                existingStock.StockNameEN = stock.StockNameEN;
                existingStock.StockNameKH = stock.StockNameKH;
                existingStock.CategoryCateID = stock.CategoryCateID;
                existingStock.updateAt = DateTime.Now.ToLocalTime();
                existingStock.updateBy = stock.updateBy;
                existingStock.StockQty += stock.StockQty;
                existingStock.UnitOfMeasureId = stock.UnitOfMeasureId;
                _contexts.Stocks.Update(existingStock);
                
                await _contexts.SaveChangesAsync();
                return true;
            }
            catch (Exception )
            {

                return false;
            }
        }

        public async Task<Stock> FindById(int id)
        {
            try
            {
                return await _contexts.Stocks.Include(_=>_.Category).Include(_=>_.UnitOfMeasure).FirstOrDefaultAsync(p => p.StockId == id);
            }
            catch (Exception )
            {
                return null;
            }
        }

        public async Task<List<Stock>> StocksList()
        {
            try
            {
                return await _contexts.Stocks.Include(_=>_.Category).Include(_=>_.UnitOfMeasure).Where(_=>_.Status=="Y").ToListAsync();
            }
            catch (Exception )
            {
                return null;
            }
        }
    }
}
