﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class CategoryRepo : ICategoryRepo
    {
        private readonly AppContexts appContexts;

        public CategoryRepo(AppContexts appContexts)
        {
            this.appContexts = appContexts;
        }

        public List<Category> CategoryList()
        {
            var category = appContexts.Category.ToList();
            return category;
        }

        public bool DeleteCate(int id)
        {
            var cateResp = appContexts.Category.Find(id);
            try
            {
                appContexts.Category.Remove(cateResp);
                appContexts.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public Category FindCateById(int id)
        {
            Category category = appContexts.Category.Find(id);
            return category;
        }

        public bool SaveCategory(Category category)
        {
            appContexts.Category.Add(category);
            try
            {
                appContexts.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public Category UpdateCate(Category category)
        {
            var existingcate = appContexts.Category.Find(category.CateID);
            existingcate.CateName = category.CateName;
            existingcate.CateNameKH = category.CateNameKH;

            appContexts.Category.Update(existingcate);
            try
            {
                appContexts.SaveChanges();
                return existingcate;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

    }
}
