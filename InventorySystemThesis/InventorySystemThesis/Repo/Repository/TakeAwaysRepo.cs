﻿using InventorySystemThesis.Models.Inventory.TakeAways;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class TakeAwaysRepo : ITakeAwaysRepo
    {
        private readonly AppContexts contexts;
        public TakeAwaysRepo(AppContexts contexts) 
        {
            this.contexts = contexts;
        }

        public bool DeleteTa(int id)
        {
            var taRepo = contexts.TakeAways.Find(id);
            try
            {
                contexts.TakeAways.Remove(taRepo);
                contexts.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public TakeAway FindTaById(int id)
        {
            TakeAway takeAway = contexts.TakeAways.Find(id);
            return takeAway;
        }

        public bool SaveTakeAway(TakeAway takeAway)
        {
            contexts.TakeAways.Add(takeAway);
            try
            {
                contexts.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<TakeAway> TakeAwayList()
        {
            var takeaways = contexts.TakeAways.ToList();
            return takeaways;
        }

        public async Task<bool> UpdateAvailable(int id)
        {
            var ta = await this.contexts.TakeAways.Where(_ => _.Id == id).FirstOrDefaultAsync();
            ta.Status = "Y";
            this.contexts.TakeAways.Update(ta);
            try
            {
                return await this.contexts.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public TakeAway UpdateTa(TakeAway takeAway)
        {
            var existingTa = contexts.TakeAways.Find(takeAway.Id);
            existingTa.Name = takeAway.Name;

            contexts.TakeAways.Update(existingTa);
            try
            {
                contexts.SaveChanges();
                return existingTa;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<bool> UpdateUnavailable(int id)
        {
            var ta = await this.contexts.TakeAways.Where(_ => _.Id == id).FirstOrDefaultAsync();
            ta.Status = "N";
            this.contexts.TakeAways.Update(ta);
            try
            {
                return await this.contexts.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
