﻿using InventorySystemThesis.Models.Inventory.Products;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class MenuItemRepo : IMenuItemRepo
    {
        private readonly AppContexts appContexts;

        public MenuItemRepo(AppContexts appContexts)
        {
            this.appContexts = appContexts;
        }

        public bool DelateMenuItem(int id)
        {
            var menuItem = appContexts.MenuItems.Find(id);
            try
            {
                appContexts.MenuItems.Remove(menuItem);
                appContexts.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public MenuItem FindMenuItemById(int id)
        {

            var menuItem = appContexts.MenuItems
                             .Include(_ => _.Stock).ThenInclude(_ => _.Category)
                             .Include(_ => _.Stock).ThenInclude(_ => _.UnitOfMeasure)
                             .Where(p => p.MenuItemId == id).FirstOrDefault();
            return menuItem;
        }

        public List<MenuItem> MenuItemList()
        {
            var menuItem = appContexts.MenuItems.ToList();
            return menuItem;
        }

        public bool SaveMenuItem(MenuItem menuItem)
        {
            appContexts.MenuItems.Add(menuItem);
            try
            {
                appContexts.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public MenuItem UpdateMenuItem(MenuItem menuItem)
        {
            var existingmenuItem = appContexts.MenuItems.Find(menuItem.MenuItemId);
            existingmenuItem.MenuItemNameEN = menuItem.MenuItemNameEN;
            existingmenuItem.MenuItemNameKH = menuItem.MenuItemNameKH;
            existingmenuItem.StockId = menuItem.StockId;
            existingmenuItem.Price = menuItem.Price;
            existingmenuItem.qtytoStock = menuItem.qtytoStock;

            appContexts.MenuItems.Update(existingmenuItem);
            try
            {
                appContexts.SaveChanges();
                return existingmenuItem;
            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }
}
