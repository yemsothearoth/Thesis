﻿using InventorySystemThesis.Helpers;
using InventorySystemThesis.Models.Inventory.Users;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class UsersRepo : IUsersRepo
    {
        private readonly AppContexts _contexts;
        public UsersRepo(AppContexts contexts)
        {
            _contexts = contexts;
        }

        public async Task<bool> DeleteUser(int id)
        {
            try
            {
                //select * from users p where p.userId = users.UserID
                var existingUser = await _contexts.Users.Include(_=>_.Roles).Where(p => p.UserID == id).FirstOrDefaultAsync();
                existingUser.Status = "N";
                _contexts.Users.Update(existingUser);
                await _contexts.SaveChangesAsync();

                return true;

            }
            catch (Exception )
            {

                return false;
            }
        }

        public async Task<Users> FindById(int userId)
        {
            try
            {
                return await _contexts.Users.Include(_=>_.Roles).FirstOrDefaultAsync(p => p.UserID == userId);
            }
            catch (Exception )
            {
                return null;
            }
        }

        public async Task<bool> FindByUsername(string username)
        {

            try
            {
                var user = await _contexts.Users.Where(_ => _.Username == username.Trim().ToLower()).FirstOrDefaultAsync();
                if (user != null)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<bool> InsertUser(Users users)
        {
            
                try
                {
                    users.Password = PassHashing.EncryptPass(users.Password);
                    users.Username = users.Username.ToLower().Trim();
                    users.loginAt = DateTime.Now.ToLocalTime();
                    users.Status = "Y";
                    await _contexts.Users.AddAsync(users);
                    await _contexts.SaveChangesAsync();

                    return true;

                }
                catch (Exception)
                {

                    return false;
                }
            
        }

        public async Task<Users> LoginUsers(Users users)
        {
            try
            {   
                users.Password = PassHashing.EncryptPass(users.Password);
                Users existingUser = await _contexts.Users.Include(_=>_.Roles).Where(p => p.Username == users.Username.ToLower().Trim() && p.Password==users.Password && p.Status == "Y").FirstOrDefaultAsync();
                if (existingUser != null)
                {
                    existingUser.loginAt = DateTime.Now.ToLocalTime();
                    _contexts.Users.Update(existingUser);
                    await _contexts.SaveChangesAsync();
                    return existingUser;


                }
                else
                    return null ;

            }
            catch (Exception )
            {

                return null;
            }
        }

        public async Task<bool> LogoutUser(int id)
        {     
                try
                {
                    var existingUser = await _contexts.Users.Include(_ => _.Roles).Where(p => p.UserID == id).FirstOrDefaultAsync();
                    existingUser.logoutAt = DateTime.Now.ToLocalTime();
                    _contexts.Users.Update(existingUser);
                    await _contexts.SaveChangesAsync();
                    return true;
                }
                catch (Exception e)
                {

                    throw e;
                }
          
        }

        public async Task<bool> UpdateUser(Users users)
        {
            try
            {

                var existingUser = await _contexts.Users.Include(_=>_.Roles).Where(p => p.UserID == users.UserID).FirstOrDefaultAsync();

                if (users.Password!=null)
                    existingUser.Password = PassHashing.EncryptPass(users.Password);
              
                existingUser.RolesID = users.RolesID;
                existingUser.Gender = users.Gender;
                _contexts.Users.Update(existingUser);
                await _contexts.SaveChangesAsync();

                return true;    

            }
            catch (Exception  )
            {

                return false;
            }
        }

        
        public async Task<List<Users>> UserList() 
        {
            try
            {
                //select * from [dbo].Users;
                //select  * from [dbo].Users where status='Y';
                // var test = await _contexts.Users.Where(p => p.Status == "Y").Include(_=>_.Roles).ToListAsync();
                return await _contexts.Users.Where(p => p.Status == "Y").Include(_ => _.Roles).ToListAsync();
            }
            catch (Exception )
            {
                return null;
            }

            
        }
    }
}
