﻿using InventorySystemThesis.Models.Inventory.Table;
using InventorySystemThesis.Models.Services;
using InventorySystemThesis.Repo.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.Repository
{
    public class TableRepo : ITableRepo
    {
        private readonly AppContexts contexts;
        
        public TableRepo(AppContexts contexts) 
        {
            this.contexts = contexts;
        }

        public bool DeleteTable(int id)
        {
            var empResp = contexts.Tables.Find(id);
            try
            {
                contexts.Tables.Remove(empResp);
                contexts.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public Tables FindTableById(int id)
        {
           Tables tables = contexts.Tables.Find(id);
            return tables;
        }

         public async Task<Tables> GetTableNumberByTblId(int id)
        {
            try
            {
                return await this.contexts.Tables.Where(_ => _.TableID == id).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool SaveTable(Tables tables)
        {
            contexts.Tables.Add(tables);
            try
            {
                contexts.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<Tables> TableList()
        {
            var tables = contexts.Tables.ToList();
            return tables;
        }

        public async Task<bool> UpdateAvailable(int id)
        {
            var tbl = await this.contexts.Tables.Where(_ => _.TableID == id).FirstOrDefaultAsync();
            tbl.Status = "Y";
            this.contexts.Tables.Update(tbl);
            try
            {
                return await this.contexts.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public Tables UpdateTable(Tables tables)
        {
            var existingtbl = contexts.Tables.Find(tables.TableID);
            existingtbl.FloorNumber = tables.FloorNumber;
            existingtbl.TableNumber = tables.TableNumber;

            contexts.Tables.Update(existingtbl);
            try
            {
                contexts.SaveChanges();
                return existingtbl;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<bool> UpdateUnavailable(int id)
        {
            var tbl = await this.contexts.Tables.Where(_ => _.TableID == id).FirstOrDefaultAsync();
            tbl.Status = "N";
            this.contexts.Tables.Update(tbl);
            try
            {
                return await this.contexts.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

    }
}
