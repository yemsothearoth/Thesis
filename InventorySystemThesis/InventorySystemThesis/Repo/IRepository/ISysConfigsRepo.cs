﻿using InventorySystemThesis.Models.Inventory.SystemConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface ISysConfigsRepo
    {
        Task<IEnumerable<SystemConfig>> SystemConfigList();
        Task<SystemConfig> GetSystemConfigByKey(string key);
        Task<SystemConfig> GetSystemConfigById(int id);
        Task<bool> AddSystemConfig(SystemConfig systemConfig);
        Task<bool> UpdateSystemConfig(SystemConfig systemConfig);
        Task<bool> DeleteSystemConfig(int id);
    }
}
