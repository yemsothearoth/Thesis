﻿using InventorySystemThesis.Models.Inventory.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface IInvoicesRepo
    {
        Task<IEnumerable<Invoices>> InvoicesList();
        Task<IEnumerable<Invoices>> InvoicesBydate(DateTime to, DateTime from);
        Task<Invoices> FindById(string id);
        Task<Invoices> FindByTableId(int tblid);
        Task<Invoices> UpdateInvoice(Invoices invoices);
        Task<Invoices> AddeInvoices(Invoices invoices);
        Task<Invoices> PaidInvoice(Invoices invoices);
        Task<bool> VoidInvoice(string id,string username);
        Task<bool> validateUpdateQty(int invoiceDetailId,float newQty);
        Task<bool> validateInsertQty(int menuItemId,float Qty);
    }
}
