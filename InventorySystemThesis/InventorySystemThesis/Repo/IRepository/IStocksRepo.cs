﻿using InventorySystemThesis.Models.Inventory.Stocks;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface IStocksRepo
    {
        Task<List<Stock>> StocksList();
        Task<Stock> FindById(int id);
        Task<bool> createNewStock(Stock stock);
        Task<bool> updateStock(Stock stock);
        Task<bool> deleteStock(int id);
    }
}
