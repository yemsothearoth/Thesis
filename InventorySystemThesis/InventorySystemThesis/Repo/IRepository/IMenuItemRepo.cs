﻿using InventorySystemThesis.Models.Inventory.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
     public interface IMenuItemRepo
    {
        List<MenuItem> MenuItemList();

        bool SaveMenuItem(MenuItem menuItem);

        MenuItem FindMenuItemById(int id);

        bool DelateMenuItem(int id);

        MenuItem UpdateMenuItem(MenuItem menuItem);
    }
}
