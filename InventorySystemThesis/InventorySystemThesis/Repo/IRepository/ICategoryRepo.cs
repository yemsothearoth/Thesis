﻿using InventorySystemThesis.Models.Inventory.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
   public interface ICategoryRepo
    {
               
        List<Category> CategoryList();

        bool SaveCategory(Category category);

        Category FindCateById(int id);

        Category UpdateCate(Category category);

        bool DeleteCate(int id);
    }
}
