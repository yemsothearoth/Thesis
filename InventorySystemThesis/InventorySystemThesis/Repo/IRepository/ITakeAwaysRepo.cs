﻿using InventorySystemThesis.Models.Inventory.TakeAways;
using InventorySystemThesis.Models.Inventory.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface ITakeAwaysRepo
    {
        Task<bool> UpdateUnavailable(int id);
        Task<bool> UpdateAvailable(int id);
        List<TakeAway> TakeAwayList();
        bool SaveTakeAway(TakeAway takeAway);
        TakeAway FindTaById(int id);
        TakeAway UpdateTa(TakeAway takeAway);
        bool DeleteTa(int id);
    }
}
