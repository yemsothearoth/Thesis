﻿using InventorySystemThesis.Models.Inventory.TakeAwaysInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface ITAInvoicesRepo
    {
        Task<IEnumerable<TAInvoices>> InvoicesList();
        Task<IEnumerable<TAInvoices>> InvoicesBydate(DateTime to, DateTime from);
        Task<TAInvoices> FindById(string id);
        Task<TAInvoices> FindByTakeAwaysId(int taid);
        Task<TAInvoices> UpdateInvoice(TAInvoices invoices);
        Task<TAInvoices> AddeInvoices(TAInvoices invoices);
        Task<TAInvoices> PaidInvoice(TAInvoices invoices);
        Task<bool> VoidInvoice(string id,string username);
        Task<bool> validateUpdateQty(int invoiceDetailId,float newQty);
        Task<bool> validateInsertQty(int menuItemId,float Qty);
    }
}
