﻿using InventorySystemThesis.Models.Inventory.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface IUsersRepo
    {   
        Task<List<Users>> UserList();
        Task<Users> FindById(int userId);
        Task<bool> FindByUsername(string username);

        Task<bool> InsertUser(Users users);
        Task<bool> UpdateUser(Users users);
        Task<bool> DeleteUser(int users);
        Task<Users> LoginUsers(Users users);
        Task<bool> LogoutUser(int id);


    }
}
