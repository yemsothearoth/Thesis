﻿using InventorySystemThesis.Models.Inventory.Users;
using InventorySystemThesis.Models.Inventory.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventorySystemThesis.Repo.IRepository
{
    public interface ITableRepo
    {
        Task<bool> UpdateUnavailable(int id);
        Task<bool> UpdateAvailable(int id);
        Task<Tables> GetTableNumberByTblId(int id);
        List<Tables> TableList();
        bool SaveTable(Tables tables);
        Tables FindTableById(int id);
        Tables UpdateTable(Tables tables);
        bool DeleteTable(int id);
    }
}
