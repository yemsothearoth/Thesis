﻿using InventorySystemThesis.Models.Inventory.Categories;
using InventorySystemThesis.Models.Inventory.SystemConfigs;
using InventorySystemThesis.Models.Inventory.UoMs;
using InventorySystemThesis.Models.Inventory.Users;
using InventorySystemThesis.Models.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Demo.SeedData
{
    public class PrepData
    {
        public static void PrepPopulation(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.CreateScope();
            SeedData(serviceScope.ServiceProvider.GetService<AppContexts>());
        }


        private static void SeedData(AppContexts contexts)
        {

            contexts.Database.Migrate();
            if (!contexts.Users.Any())
            {
                contexts.Users.AddRangeAsync(

                    new Users {
                        Username = "admin",
                        Password = "40BD001563085FC35165329EA1FF5C5ECBDBBEEF",
                        Status = "Y",
                        RolesID = 1,
                        Gender = "Male",
                        loginAt = DateTime.Now.ToLocalTime(),
                        logoutAt = DateTime.Now.ToLocalTime()
                    },

                    new Users
                    {
                        Username = "user",
                        Password = "40BD001563085FC35165329EA1FF5C5ECBDBBEEF",
                        Status = "Y",
                        RolesID = 2,
                        Gender = "Male",
                        loginAt = DateTime.Now.ToLocalTime(),
                        logoutAt = DateTime.Now.ToLocalTime()
                    }
                    );
            }
            if (!contexts.Roles.Any())
            {
                contexts.Roles.AddRangeAsync(
                    new Roles
                    {
                        RolesName = "Admin"
                    },
                    new Roles
                    {
                        RolesName = "Users"
                    }

                    );
            }
            if (!contexts.Category.Any())
            {
                contexts.Category.AddRangeAsync(
                    new Category
                    {
                        CateName = "Food",
                        CateNameKH = "ម្ហូប"
                    },
                    new Category
                    {
                        CateName = "Soft Drink",
                        CateNameKH = "ភេសជ្ជៈ"
                    },
                    new Category
                    {
                        CateName = "Beer",
                        CateNameKH = "ស្រាបៀរ"
                    },
                    new Category
                    {
                        CateName = "Wine",
                        CateNameKH = "ស្រា"
                    }

                    );
            }
            if (!contexts.UnitOfMeasures.Any())
            {
                contexts.UnitOfMeasures.AddRangeAsync(
                new UnitOfMeasure { NameEN = "g", NameKH = "ក្រាម" },
                new UnitOfMeasure { NameEN = "can", NameKH = "កំប៉ុង" },
                new UnitOfMeasure { NameEN = "bottle", NameKH = "ដប" },
                new UnitOfMeasure { NameEN = "plate", NameKH = "ចាន" }

                    );
            }
            if (!contexts.SystemConfigs.Any())
            {
                contexts.SystemConfigs.AddRange(
                new SystemConfig { Key = "EXCHANGE_RATE", Value = "4000" },
                new SystemConfig { Key = "INVOICE_PREFIX", Value = "INV" },
                new SystemConfig { Key = "TA_INV_PREFIX", Value = "TAINV" }
                );
            }
            contexts.SaveChanges();
        }
    }
}